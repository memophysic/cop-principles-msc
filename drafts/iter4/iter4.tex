\title{Metrics for adaptation space coverage evaluation}
\author{Samuel Longchamps (\url{samuel.longchamps@usherbrooke.ca})}

\documentclass[10pt,twocolumn]{article}
\usepackage{../itersummary}
\addbibresource{../COP-MSC.bib}

\begin{document}\twocolumn\maketitle
\section{Second part of research project}
The second part of the project has two goals: develop a more complete model for the monitoring of parameters in adaptive software and propose new metrics which will assist developers when designing adaptive software

The model for monitoring proposed in the first article focused on enumerated value domains. Enumerated domains are usually rather small in number of values (\(< 100\)) and can therefore be easily modeled using sets. Other domain types are discrete and continuous. A discrete domain is akin to an enumerated one where values are generated from a specific range between minimum and maximum values and every possible value must lie on a specific scale obtained by using a step. Large domains can therefore efficiently be expressed, that is there is no need to keep in memory all the values: only the limits and the step. Continuous domains have limits much like discrete domains, but do not constrain values to lie on a certain scale. Continuous domains do not have a finite number of elements and are therefore always expressed using floating-numbers. These three types of domains can be used to model various domains of parameter values.

The hypothesis supporting the use of such a model is that we expect to reduce the complexity of computations at a system level by giving the responsibility of adaptation assurance to components. Adaptive components define a range of domains for certain parameters they support and assure to the system that their behavior will be correct in states lying on the domains. A composite component (a component made-up of other components) will then have the responsibility to assure that its underlying parts behave correctly within the union of their supported adaptation space. If not, it will need to provide the states within which it can be expected to behave correctly (constrain the unionized adaptation space). This separation of responsibility divides the problem of assurance into smaller sections of at most two abstraction or composition levels. Arbitrarily large composites can be safely designed by composing and assuring each layer, one at a time.

The use of a more complete model for parameter monitoring provides an opportunity to develop metrics based on accessible data. Since components are able to express the domain of supported parameter values, it is possible to compute the coverage of a component's adaptation space over the modeled parameter space. This value indicates how specialized a concrete component is (smaller coverage implies more specialized). In the case of composite components, it allows for the designer to know if some state will be unsupported by the component. He can then take action, for example by adding more candidates to support missing states. This metric is important because a composite component is implicitly expected to support the union of the adaptation spaces of its parts, but this union adds states not supported by any of its parts when multiple parameters are defined. This metric will indicate if such states are induced by its parts.

\section{Coverage metric}
The standard structure for adaptive components links each component to a ``Parameter Value Provider'' (PVP). This artifact is responsible for providing a value for each parameters an adaptive component defines adaptation over i.e. parameters in its adaptation space. At runtime, a monitor provides values for a single parameter. A monitor can provide any values in the domain defined by the parameter, but also define only a subset of said parameter. This leads to two domains of possible values for adaptation to occur: one statically defined by parameters and another one dynamically defined by monitors upon selection to be part of the PVP of an adaptive component.

Coverage can be expressed as the percentage of the adaptation space of a component over the reachable adaptation space, either defined by parameters as a  \emph{modeled space} or by monitors as an \emph{effective space}. This ratio is computed rather trivially for a non-composite component. An adaptation space is formed by pairs of state space definitions \({\omega_P}_i\) and its parameter \(P_i\). The number of combinations of defined values for each parameter \(i\) forms the reachable state space for this component. At a system level, a similar computation can be done to obtain the number of reachable states for the parameters the component defines. The state spaces are in this case all the possible values of a parameter \(P\) or a monitor \(M\): \({\Omega_{\{P,M\}}}_i\).

The metric's computation for a non-composite ``primitive'' component is summarized in equation \ref{eq:coverage}.

\begin{equation}
  C_{primitive} = \frac{|{\omega_P}_i|}{|{\Omega_{\{P,M\}}}_i|}
  \label{eq:coverage}
\end{equation}

Unless manually specified, composite components have their state space defined as the union of the state spaces \(\gamma\) of the \(N\) components it is made of. As previously mentioned, this leads to the specification of some supported states which were not explicitly defined in candidates when multiple parameters are involved. A way to accurately compute this metric is therefore to compute the coverage \(C_k\) of individual substitution candidates \(k\), sum them and subtract the coverage of all the intersecting candidates' state space \(C_{\bigcap{\gamma_{0 \dots N}}}\). This subtraction is necessary because states defined in more than one component would otherwise be counted multiple times. This is summarized in equation \ref{eq:composite-coverage}.

\begin{equation}
  C_{composite} = \left( \sum_{k=0}^{N} {C_{primitive}}_{k} \right) - C_{\bigcap{\gamma_{0 \dots N}}}
  \label{eq:composite-coverage}
\end{equation}

Typically, as the number of parameters and state spaces grows, such computations would quickly escalate as to reach the computational limit. To prevent this, implementations of state spaces can be devised to define these state spaces lazily and compute the variables needed by the metrics without the need to explicit every state or keep the in memory. A lazy implementation is planned for two of the three types of state spaces: discrete and continuous. Because enumerated state spaces are expected to be quite small (<< 100 elements) and each state expressed explicitly, the proposed approach was impractical.

The remaining challenge is to compute the expression \(C_{\bigcap{\gamma_{0 \dots N}}}\). Because we are required to find duplicates of states, one obvious and slow solution is to go through the possible states and count how many components support it: if it is more than 1, add to the duplication count. The numerator is then divided by a denominator representing the total number of possible states. Another method, which would require some work, would be to compute an intersection adaptation space between each pair of components, count the number of elements and add them to the numerator. An evaluation of the complexity required to generate those new state spaces versus going through all states is necessary to devise an efficient solution.

\section{Developments}
A prototype of the metric's computation for both modeled and effective coverage was developed during the course of this iteration. Support of enumerated and discrete state spaces has been tested while continuous ones are yet to be implemented. There is also a complete support of primitive and composite components with an option to compute a composite explicitly as a primitive component (useful if the adaptation space was manually overridden). Some sample programs with an implementation of adaptive components based on a previous iteration (zoo animal adaptive caging) were created for testing purposes. The prototype shows very satisfying performances for simple cases (\(< 1\) sec computation for all components).

The code is accessible at the following link: \url{https://gitlab.com/memophysic/adaptive_test_project/tree/develop/metrics}

Most of the time was spent on devising a way to efficiently compute the metric for any type of component which define their adaptation space with any type of state space. This was possible by the implementation of a StateSpace interface in AdaptivePy which replaces the dictionary approach used until now for specifying state spaces. The new data structure offers complete encapsulation for state spaces and is used in all the methods for adaptation space handling. The only thing that remains mostly the same is the dictionary structure used to define an adaptation space. This dictionary has parameters for keys and state spaces for values. Another addition to AdaptivePy was the realization that monitors can be thought as parameters in that they provide a state space of possible values. This allows the implementation of the coverage metric computation to use monitors as keys for an adaptation space and therefore to compute the effective coverage with the same generic code as modeled coverage.

\section{Further work}
The next goal is to complete the implementation of continuous state spaces and assure that the computation of the metric is working correctly with this state space type. The biggest challenge is probably to reconcile the approach of going through each states. This is obviously not possible for continuous state spaces as there is an infinite number of states between two values.

Also, the goal of the next iteration would be to have a version of AdaptivePy stable enough to start writing the article about the metrics. Some more unit tests will be needed and I will need to think about how the functions for the metric's computation should be published (as part of AdaptivePy, or as an additional library). Perhaps a small GUI tool to assist developers could be developed if I have spare time. It would load a Python module, look for classes which define ``has\_adaptation\_space'' and run the metric on them, listing the results in a grid.

% \printbibliography

\end{document}
