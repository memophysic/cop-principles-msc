%\title{Design patterns for addition of adaptive behavior in component-based graphical user interfaces development}
\title{Design pattern for monitoring adaptive data in component-based software design applied to graphical user interfaces development}
\author{Samuel Longchamps (\url{samuel.longchamps@usherbrooke.ca})}

\documentclass[10pt,twocolumn]{article}
\usepackage{../itersummary}
\addbibresource{../COP-MSC.bib}

\begin{document}\twocolumn\maketitle
\section{Introduction}\label{sec:intro}
Adaptive software has become, in the last decade, a research subject of great interest as software systems have become increasingly complex and distributed. With the variety of platforms, requirements for applications to not only work on many of them, but also to adapt their human interfaces to each of them has become a major challenge. Adaptation in software implies that its life-cycle is not stopped after its development and initial setup \cite{salehie2009}. This large spectrum along with the importance of tradeoffs between flexibility, complexity and performance have led scientists and engineers to investigate and characterize solutions such as conceptual frameworks to simplify the development of adaptive software. Additionally, adaptive behavior cannot fully be planned from the beginning, therefore there is a need for techniques to integrate it gradually throughout the development process.

Graphical user interfaces (GUI) are very often required to adapt themselves to provide the best experience to users. Although many meta-models and conceptual frameworks have been proposed to implement adaptive behavior in a generic manner (\cite{Chang2001} \cite{bruneton2006fractal} \cite{Maurel2010} \cite{Peissner2011}), techniques to concretely implement adaptive behavior in GUI has received minimal attention.

The goal of this paper is to propose a design pattern to implement adaptive behavior in a component-based architecture such that the software stays scalable and maintainable. Design patterns will enforce separation of concerns in order to enable scalability, long-term extensibility and reusability. In opposition to concrete frameworks which typically require overall architectural shift, the design pattern must be applicable on certain specific components and be incrementally used to transform a static system to an adaptive one as adaptive requirements are elaborated and formulated by the client over the course of the software development process.

\section{Motivation}\label{sec:motivation}
Even if GUI code generally has for concern the customization of pre-made components provided by a toolkit, it can account for an important portion of an application's codebase. Components-based software engineering implemented for GUI in the form of toolkits has been the de facto mean to promote reusability and ease of design in GUI. Although adaptation can be done to a certain extent by these toolkits (e.g. support of multiple operating systems, rendering engines, etc.), concrete lay outing and users' domain-specific behavior in GUI are concerns which are application-specific and cannot be integrated in toolkits. Too often, this code is highly tangled to specific GUI implementations. This limits their reusability in modern applications (e.g. multiple views), resulting in duplication of customization code of functionally equivalent components, for example when porting the application to another platform or toolkit. On the long term, adaptation requirements for a given application grow larger and complexifies customized components. Some consequences are that refactoring and extension of components becomes harder to accomplish and to validate in all target adaptation contexts.

To solve this problem, an additional abstraction layer can be used, but there are still limitations. In the case of GUI, graphical design tools and frameworks are specific at least to a toolkit's component and to certain platforms and programming/descriptive language. This layer and its implementation details can be encapsulated and hidden from the developer if the form of a framework, enforcing some extensively designed meta-model for specification of the application's architecture. Although a valid solution, this approach doesn't directly support the developer in understanding the mechanisms of adaptivity and can remove necessary control over its application from the developer's hands. Also, the various constructs used by the framework can be alien to many developers (implying a steep learning curve) and hard to integrate with existing components. The integration of a framework, even with the flexibility of a conceptual one, can result in major efforts in rewriting code tangled to application logic and the GUI. This cost can be too large to be realizable and one might want to support this decision by evaluating gains in extendability and scalability against costs in eventual tangling to the framework's modules or architecture. Because this analysis would too require a significant effort, an alternative solution which is both simpler to implement incrementally and evaluate in terms of costs is needed. More specifically, there is a need for solutions to transform an existing static application to an adaptive one iteratively and with minimal effort.

Our goal is to provide some solutions in the form of design patterns (established solutions to common software problems), with no pretension of the presented set to be exhaustive. They aim at improving separation of concerns and reusability, resulting in a minimization effort for gradual integration of adaptive behavior. They also aim at reducing legacy and duplicated code at every stage of software development through their integration to the refactoring process rather than an overall rewriting of large parts of an application. Rapid prototyping should stay as fast and simple, eventually faster with the reuse of existing solutions for adaptation mechanisms.

% Some requirements have been identified to which the pattern must comply to attain the necessary level of reusability, iterative integration and ease of implementation:
%
% \begin{enumerate}
%   \item Control must be decentralized in order for components to adapt themselves on partial knowledge of their context
%   \item Adaptation mechanisms should be reusable in many components
%   \item Multiple components should be able to coordinate one another (conflict resolution)
%   % \item More?
% \end{enumerate}

%
% The cost being spread over many iterations and applied only when needed makes its use more generally practical.
%
% To software developers and engineers, self-adaptation mechanisms can be used to improve one or many of a quality factors such as performance, quality of service (QoS)

\section{Concepts of software adaptation}
\subsection{Context and self adaptation data}\label{sec:context-self-adaptation-data}
The goal of adaptation is to customize a system such that its provided solutions are as specific as possible to the related problems' context. Data used to make these customizations, called "adaptation data", can come from various sources, both external and internal to an application. A system using external data for adaptation would be considered self-situated while one using internal data would be considered self-aware \cite{hinchey2006self}. Authors also refer to self-situated systems as context-aware, where context is the operational environment \cite{Parashar2005} \cite{salehie2009}. Context-aware will be used in this paper rather than self-situated since its usage appears more popular among researchers.

The context of a software refers to the environment in which it is executed. Example contextual data are the geographical position, light intensity, temperature, but also the platform on which that application is executed. A computing platform can be composed of many parts such as hardware components, operating system, computing capability and, in the case of GUI, user-interface toolkit \cite{majrashi2015}. Contextual data is usually acquired using specialized sensors/probes which are monitored by software program counterparts.

Self-awareness is a basic requirement for self-adaptivity since it is through a representation of itself that a system can deduce how it satisfies given constraints and modify itself to better satisfy them. Self-awareness can be achieved through self-monitoring of a system's components by software entities as it is the case for autonomic managers in the MAPE-K model.

Given these two types of adaptation data, we consider a system fully adaptable if it can both adapt based on monitored contextual and self data.

\subsection{Types of adaptation}\label{sec:types-adaptation}
Four widely accepted types of adaptation concerns or objectives have been proposed by \citet{hinchey2006self}: self-configuration, self-healing, self-optimization and self-protection. Different qualities a system must have to enable these objectives are to be self-aware, self-situated, self-monitoring and self-adjusting. While some concrete solutions have been proposed for self-optimization and self-healing \cite{Weyns2010}, our main concern for the design of graphical user interface is self-configuration. We synthesize two prominent types of adaptation schemes for self-configuration used in component-based software engineering: parametric adaptation and component substitution.

\paragraph{Parametric adaptation}
A component can expose means to tune its configuration through a "tunable interface". A unit of configuration which can be tuned is called a knob \cite{Cabri2011}. This interface can be part of a component's interface or be automatically generated at the meta-programming level (e.g. using annotations \cite{salehie2009}). A common example of this adaptation pattern is to choose among different implementations of an algorithm, each providing a different set of quality values (performance, precision, etc.). As such, domains of tunability for each configuration parameter are explicit and can vary over time. For example, if a new algorithm is discovered in the middle of a large computation, an adaptation mechanism aware of the available options at adaptation time is able to switch to it.

\paragraph{Component substitution}
Like an algorithm in parametric adaptation, one among multiple components which satisfy certain expected features might be selected at runtime to accomplish them. At some point in time, an adaptation mechanism can decide that another component is preferable over the currently selected one and swap them. For example, a checkbox typically used in desktop GUI where a mouse is used as pointing device might be swapped for a more convenient switch associated to touch input as the user has started using the touch panel rather than the mouse.\\

Our first proposal is that both types of adaptation are complementary and is summarized in statement \ref{eq:complementary-adaptation-types}.

\texteq{Any parent of components with substitution candidates gain an equivalent degree of parametrization.}{eq:complementary-adaptation-types}

A composite component can therefore substitute its child components and decide to provide to its own parent entities implicit control over the substitution by means of parameters. A key guideline for this design to be effective is that the parametrization of a component should be in the terms of its abstraction layer. This way, the parent component remains unaware of lower abstractions implementation details. Also, it should be stressed that a degree of parametrization doesn't imply control over this parametrization; the composite component has the ability to provide or not control and decide how it is provided.

Building on the button vs. switch example, we can see that the implications of this feature is that a composite component at a more abstract level is able to work with a concrete component provided as appropriate for the current context and customize the behavior of its sub-components using explicit parametric adaptation. The mean by which composite components provide parametric control over their composition can easily be realized by using the adapter pattern to adapt the concrete component to a common interface or the decorator pattern to implement the missing adaptation logic.
% Once done, say it will be demonstrated in X section

\subsection{Adaptation data acquisition and analysis}
There are already many terms and techniques proposed to acquire adaptation data, but they are mainly focused on self-adaptive systems. A major contribution is the approach used in Rainbow framework from \citet{garlan2004rainbow} which introduces \emph{probes} as modules observing and measuring various system states. The data acquired is then aggregated using specialized modules called \emph{gauges} which are responsible for updating properties in the system's architectural model, the self-representation in term of components of the system. These constructs inspire us by providing a hint toward usage of reusable component-based data acquisition and adaptation effectors. One limitation is the requirement from the framework to have a model manager to provide information about the system's architectural model. This component acts in a centralized manner, but in a decentralized context partial knowledge is assumed and therefore no one manager can be used.

\section{Adaptation primitives}
\subsection{Definitions}
\paragraph{Adaptation parameter}
An adaptation parameter is one type of adaptation data. Its possible values domain is explicitly defined.
% Explain more the different types of domains

\paragraph{Adaptivity space}
The adaptivity space of a component is set of all domain of each adaptation parameter for which a component can adapt itself to. For example, a component could have be defined for two different adaptation parameters, each defined by boolean values. One space could be that the component is defined for both states of the first parameter, but only one of the second. An empty or undefined adaptivity space is considered agnostic to all adaptation parameters, i.e. it is defined, for all values of all adaptation parameters, but will not adapt itself.

\subsection{Monitors}\label{sec:monitors}
As explained in \ref{sec:context-self-adaptation-data}, adaptation data from various sources need to be monitored in order to realize the appropriate actions. We propose that this concern be addressed in a sub-component called \textbf{monitors}. The goal of a monitor is summarized in statement \ref{eq:monitor-goal}.

\texteq{A monitor has for goal to provide a value for one adaptation parameter.}{eq:monitor-goal}

Since adaptation parameters have explicitly defined domains, so do monitors. As a requirement, monitors then need to have as defined domain a subset of the adaptation parameter it monitors. Inspired by service-oriented architectures, we propose two types of primitive monitors: static monitors and dynamic monitors. Our design pattern proposal for such monitors is shown in figure \ref{fig:monitors-uml} and further explained below.

\begin{figure}
  \includegraphics[width=\linewidth]{monitors_uml}
  \label{fig:monitors-uml}
  \caption{UML diagram of adaptation data monitors}
\end{figure}

\paragraph{Static monitor}
A static monitor uses the Request/Response scheme to provide a value for an adaptation parameter. A static monitor needs to be \textbf{stateless}, which implies that each response has to be fully constructed after a request has been received. The logic for acquiring the value is encapsulated in this component and can necessitate other monitors. To conserve statelessness though, a static monitor can only be an aggregation of other static monitors or monitors which have been decorated by optional dynamic features\footnote{This feature can be guaranteed by contract, provided that the dynamic monitor can act in a stateless context, that is when it is not started.}. For example, the principle of the facade design pattern can be implemented to provide a simpler domain of values for a higher level abstraction by aggregating other adaptation parameters.

\paragraph{Dynamic monitor}
A dynamic monitor uses a scheme we call Notify/Update based on the Observer pattern (as illustrated by the Observable class in figure \ref{fig:monitors-uml}) where a component can register itself to the monitor and receive notifications when the adaptive parameter's value has changed. Some form of polling or interrupt-based thread awakening needs to be employed along with a previous value to decide when to notify registered components. This property makes dynamic monitors inherently \textbf{stateful}. Multiple notification rules could be used, but our design enforces that a notification must come from a transition from a context value to another one in the domain it explicitly defines. Key properties of this monitor is that it can accumulate data over time, filter state changes based on the history of acquired values, be activated (started) or deactivated (stopped) on-demand and aggregate both static and dynamic monitors.\\

The implications of this design pattern is that an arbitrarily complex monitoring system can be constructed from just these two primitive types. Adaptive components, as it will be discussed further, monitors will implicitly be used to acquire values needed to accomplish their parametric adaptations based on the values of adaptation parameters. Their ability to inform the system of the value they can provide at runtime allows for adaptation of the monitoring strategy, while the support for both static and dynamic behavior allows for various level of control flow at the application level. A single component can request to some authority (e.g. local context provider object) a monitor for an adaptation parameter and use it to drive its parametrization (see section \ref{sec:adaptive-components}).

A strategy for a widely reusable library of such monitors is to provide a library of static monitors for well-established adaptation data along with various dynamic monitoring decorators. Custom monitors using any application-specific abstraction can be easily composed and used to accomplish adaptation and control when this adaptation takes place. Many strategies and algorithm for triggering schemes have already been proposed (events and triggers \cite{Liu2006} \cite{goderis2008separation} \cite{Maurel2010}), but no englobing elements to effectively implement in CBSE has been proposed to our knowledge. This is a simple and effective attempt to implement in an iterative way adaptive behavior to specific components.

\subsection{Adaptive components}\label{sec:adaptive-components}
Section \ref{sec:types-adaptation} presents two types of adaptation: components parametrization and substitution. The main difference between parametrization compared to customization related to an application's business logic is subtle and can be subjective to a certain point. In many applications not using patterns proposed in this paper, customization is done on the basis of information we consider nevertheless as adaptation data (location, time of the day, etc.). The difference is that components do not usually provide means of knowing the domain of each adaptation data they cover in order to parametrize themselves. Our requirement for a component to be parametrically adaptive is therefore for it to \emph{provide a mean to obtain which adaptation parameter and related domain it is defined for}. This requirement aims at sharing the knowledge of the adaptivity space of a component. Otherwise, all adaptive behavior included in the component is considered part of its application logic and agnostic to any adaptation data from the point-of-view of other components. Also, the means by which any adaptation data other than monitors is the component's responsibility and cannot be socially provided by connected components.

Following design decisions of \cite{Cabri2011}, we chose to make capabilities and limits of adaptation mechanisms explicit, similarly to how we made explicit adaptation data monitoring and its domain of application. This allows our components to have insight on its actual degree of adaptability depending on the monitors' domain compared to its own. Also, because adaptive components are required to provide a mean to acquire knowledge of their adaptivity space, a composite component's adaptivity space can easily be made a superset of all its sub-components' adaptation space. This feature can be implemented easily as the composite component has an explicit mean to acquire this knowledge. A nice consequence of this design is that this principle can be applied hierarchically to build arbitrarily complex adaptive components.

Another aspect presented in section \ref{sec:types-adaptation} is the complementary nature of parametrization and substitution of components. Composite components with control over their sub-components parametrization, specified by a common interface for example, can in their turn offer parametrization to their parent component which will impact their composition. Their composition can be simply implemented as substitution and this scalable design pattern allows for construction of adaptive components of arbitrary complexity in a bottom-up incremental way.

Because components are parametrize on the basis of the provided values by monitors, there are two schemes of adaptation flow: bottom-up and top-down. This is related to the type of monitors. A static monitor necessitates the component to query a value and therefore adaptation control can be triggered from the top and propagated down to child components, e.g. by asking them to query their monitors in turn for update. In the case of dynamic monitors, they will notify registered components of a parameter value change and components will be able to propagate this action bottom-up because of the dependence on adaptation parameters they exhibit. Even more interesting, both approaches can be mixed; a component in a middle layer can propagate adaptation triggers bottom-up and choose to tune its child components through their parametrization interface. They could also request them to update themselves in the case when a child component uses a static monitor for which a parent has a dynamic version of (decorator example from \ref{sec:monitors}).

%\section{Related work}
% DEUCE
% Control loop architectural patterns (Cabri2011)

\section{Future work}
Future work will focus on implementation and formalization of a design pattern for multi-layer abstract and adaptive components based which will use the monitor pattern presented in the current paper, again in the context of GUI development. The basis presented in section \ref{sec:adaptive-components} for such components provides a good foundation, but the main concern will be to accompany developers using existing GUI design software tools to facilitate implementation of the design pattern gradually and with minimal effort.

\printbibliography

\end{document}
