\subsection{Proxy Router Pattern}\label{sec:dp-proxy-router}\noindent
\textbf{Classification:}
Plan and execute.\\
\textbf{Intent:}
A proxy router allows to route calls of a proxy to a component chosen among a set of candidates using a designated strategy.\\
\textbf{Motivation:}
When implementing component substitution, a way to clearly separate concerns relating to the adaptation logic (substitution by which component) and the execution of substitution (replacing a component or forwarding calls to it) are difficult to implement in an extensible way. The proxy pattern \cite{GoF1995} allows to forward calls to a designated instance, but does not specify how control of the routing process should be implemented. Candidate components need to be specified in a way that does not necessitate immediate loading or instantiation and which is mutable (to allow runtime discovery). To maximize reusability, strategies should be devised externally.\\
\textbf{Structure:}
Fig. \ref{fig:proxy-router-uml} shows the structure of the proxy router pattern as a UML diagram.
\begin{figure}[h]
  \includegraphics[width=\linewidth]{img/proxyrouter}
  \caption{Proxy router pattern UML diagram}
  \label{fig:proxy-router-uml}
\end{figure}~\\
\textbf{Participants:}
\begin{itemize}
  \item \textbf{Proxy:} Gang of Four \cite{GoF1995} proxy pattern, with the exception that the interface is not necessarily specified (e.g. forwarding to introspected methods).
  % Forwarding is preferably dynamic by introspection of the proxied object's methods. If not, a proxy class must be specified which inherits the interface and implements forwarding (this could be automatically generated).
  It is responsible for making sure no calls are lost when a new delegate is set.
  \item \textbf{Delegate component:} Concrete component which is proxied. It must be specified as part of the proxy router's candidates set.
  \item \textbf{Proxy router:} Keeps a set of component candidates and allows to control the routing of the calls a proxy receives to the appropriate candidate chosen by some strategy. The proxy router is responsible for ensuring any state transfer and initialization of candidate instances.
  \item \textbf{Candidate factory:} Gang of Four \cite{GoF1995} factory pattern for a candidate. Used as part of candidates definition. Can do local loading/unloading for external candidates.
  \item \textbf{Choose route strategy:} Concrete strategy to choose which candidate among a set to use, based on Gang of Four \cite{GoF1995} strategy pattern. It uses accessible information from the application, candidates (e.g. adaptation space, descriptor, static methods) or any inference engine available to make a choice.
  % Additional information can be provided by the caller as necessary. A strategy can choose only one candidate, but other strategies can be used to filter the initial set to a smaller one.
  \item \textbf{External/Internal proxy router:} Depending on the use, a proxy router can \emph{use} an external proxy (as a member) or internally \emph{be} a proxy (through inheritance).
  % Using an external proxy has the advantage of fully decoupling the route control logic from the proxy logic, while using an internal one allows to use the proxy router directly as a component.
  To allow for both schemes, a means to acquire the proxy is provided and returns either the member object (external) or a reference to the proxy router itself (internal).
\end{itemize}
\textbf{Behavior:}
A set of candidates is either statically specified or discovered at runtime (e.g. looking for libraries providing candidates).
% A strategy can also be specified statically or generated at runtime by some factory.
The proxy router is then initialized by choosing a candidate using the strategy and controls the proxy to set an instance of the chosen candidate as active delegate. At any time, a new candidate can be chosen and set as active delegate of the proxy.
% In the case of GUI, a proxy router can easily be realized using a container widget. Because the container would inherit identical base methods from being a widget, an external proxy might be preferable for complex components.
\\
\textbf{Consequences:}
The proxy router pattern allows for flexible and extensible specification of component substitution. The strategies to choose a candidate to route to can be reused in any project with consistent information acquisition infrastructure, such as the one provided by the monitor pattern. Candidates need not be specified statically and control related to routing can be done both internally and externally.\\
\textbf{Constraints:}
% Because of both types of proxy router (internal/external), it is preferable to acquire the proxy instance before working with it. When solely using internal proxies though, it is arguably acceptable to not do so and assume the interface is provided by contract.
Strategies might rely on certain project specific information which is not portable. Separating specific from generally applicable strategies and composing them should help with this constraint.\\
\textbf{Related patterns:}
Adaptive component \cite{Chen2001},
virtual component \cite{Corsaro02virtualcomponent},
master-slave \cite{Gomaa2004},
component insertion/removal, server reconfiguration \cite{ramirez2008design},
proxy \cite{GoF1995}.
