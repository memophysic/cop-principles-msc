\section{Results}\label{sec:results}
The sample program presented in \ref{sec:development} has been implemented using AdaptivePy. The three primitive adaptive components along with a composite adaptive component have been used as input for the metrics computation function, both as modeled and effective. Monitors were created to provide a single random value among the possible values of their parameter. The results are presented in table \ref{tbl:sample-coverage}. From these results, we can see that each substitution candidate has partial coverage, with Barn being the most flexible since it supports more than half of the states compared to a quarter for Cage and VE. Interestingly, Composite has a coverage of 92.5\%, while the sum of its candidates yields \(25 + 52.5 + 25 = 102.5\)\%. This means that there is \(102.5 - 92.5 = 10\)\% of recurrent coverage. This recurrent coverage is due to cumulative overlap of substitution candidates' adaptation space. This property can be visualized by using parameters as dimensions on a graph. Because we have only two parameters, we obtain a 2D graph shown in Fig. \ref{fig:sample-coverage-graph}. We see from this figure that Barn and VE overlap from 10\textdegree~ to 30\textdegree~ and \{small, medium\} animal sizes.
It can also be seen that the remaining 7.5\% percent to be defined in Composite to attain 100\% is temperatures between 30\textdegree~ and 60\textdegree~ for \{large\} animal size.

\begin{table}[h]
\centering
\caption{Coverage of sample program with monitors over full domain}
\label{tbl:sample-coverage}
\begin{tabular}{c|c|c|c||c|}
\cline{2-5}
                                & Cage & Barn   & VE & Composite \\ \hline
\multicolumn{1}{|c|}{Modeled}   & 25\% & 52.5\% & 25\%             & 92.5\%    \\ \hline
\multicolumn{1}{|c|}{Effective} & 25\% & 52.5\% & 25\%             & 92.5\%    \\ \hline
\end{tabular}
\end{table}

\begin{figure}[h]
  \includegraphics[width=\linewidth]{img/sample-coverage-graph}
  \caption{Coverage of substitution candidates for sample program with monitors over full domain}
  \label{fig:sample-coverage-graph}
\end{figure}

The results of the analysis of the sample program provide information regarding the correctness of the composite adaptive component. Such analysis is also a way for developers to evaluate the validity of their implementation. Table \ref{tbl:sample-coverage} shows that there is a need for action to be taken in order to make the composite component comply with the assumption of supporting the full adaptation space of its parts. Some actions are discussed in the following.

\paragraph{Add substitution candidate}
The undefined part could be supported by an additional substitution candidate, for example a ``HeatedArea'' which would support AS=\{large, gigantic\}, T=[10, 60, 0.5]. This would lead to the results in table \ref{tbl:sample-coverage-heatedarea}. We see that the coverage for the composite reaches 100\%, while the additional HeatedArea adaptive component has 20\% coverage. Modeled and effective coverage are still equal since monitors' possible values have not been changed. Figure \ref{fig:sample-coverage-graph-heatedarea} shows how the HeatedArea fills the gap and leads to a complete support of the context space.

\begin{table}[h]
\centering
\caption{Coverage of sample program with HeatedArea substitution candidate}
\label{tbl:sample-coverage-heatedarea}
\begin{tabular}{c|c|c|c|c||c|}
\cline{2-6}
                                & Cage & Barn   & VE & HeatedArea & Composite \\ \hline
\multicolumn{1}{|c|}{Modeled}   & 25\% & 52.5\% & 25\%             & 20\%      & 100\%      \\ \hline
\multicolumn{1}{|c|}{Effective} & 25\% & 52.5\% & 25\%             & 20\%      & 100\%      \\ \hline
\end{tabular}
\end{table}

\begin{figure}[h]
  \includegraphics[width=\linewidth]{img/sample-coverage-graph-heatedarea}
  \caption{Coverage of substitution candidates for sample program with HeatedArea substitution candidate}
  \label{fig:sample-coverage-graph-heatedarea}
\end{figure}

\paragraph{Reduce the effective context space}
The undefined part could be made unreachable at runtime by reducing the monitors' possible values to a smaller viability zone. In this case, the temperature monitor could be limited to a maximum of 30\textdegree. An alternate solution is to limit the animal sizes to \{small, medium, gigantic\}. Using the first solution leads to the results in table \ref{tbl:sample-coverage-limited-monitors}. We see that effective coverage has changed in different ways for each component. Specifically, Cage and VE have decreased in effective coverage since parts of the state they defined overlapped with the constrained states. On the other hand, Barn's effective coverage has increased because the reachable state has been decreased and the monitor's constrained states were not part of the Barn's adaptation space. Note that the modeled coverage remains the same, meaning that there are still undefined states by the composite, but that they won't be reached at runtime when using the currently provided monitors. Figure \ref{fig:sample-coverage-graph-constrainedmonitor} shows the reduced context space where the semi-transparent region on the right illustrates the remaining modeled space. We see that the reduced context space is fully covered without modification to the components when interpreted for runtime.

\begin{table}[h]
\centering
\caption{Coverage of sample program with monitors over constrained domain}
\label{tbl:sample-coverage-limited-monitors}
\begin{tabular}{c|c|c|c||c|}
\cline{2-5}
                                & Cage & Barn   & VE & Composite \\ \hline
\multicolumn{1}{|c|}{Modeled}   & 25\% & 52.5\% & 25\%             & 92.5\%    \\ \hline
\multicolumn{1}{|c|}{Effective} & 14.28\% & 75\% & 14.28\%            & 100\%    \\ \hline
\end{tabular}
\end{table}

\begin{figure}[h]
  \includegraphics[width=\linewidth]{img/sample-coverage-graph-constrainedmonitor}
  \caption{Coverage of substitution candidates for sample program with monitors over constrained domain}
  \label{fig:sample-coverage-graph-constrainedmonitor}
\end{figure}

\paragraph{Extend a substitution candidate's adaptation space}
Support for the undefined part could be added to a substitution candidate by subclassing one of them and redefining its adaptation space. For example, a ``JumboCage'' could be created as a subclass of Cage and support the large animal size. Results are shown in table \ref{tbl:sample-coverage-extended}. We see that the JumboCage now covers 37.5\% instead of 25\% and that the composite attains 100\% coverage. Figure \ref{fig:sample-coverage-graph-extended} shows how the entire modeled state space is covered by the extension of the cage component's adaptation space.

\begin{table}[h]
\centering
\caption{Coverage of sample program with monitors over constrained domain}
\label{tbl:sample-coverage-extended}
\begin{tabular}{c|c|c|c||c|}
\cline{2-5}
                                & JumboCage & Barn & VE & Composite \\ \hline
\multicolumn{1}{|c|}{Modeled}   & 37.5\% & 52.5\% & 25\% & 100\% \\ \hline
\multicolumn{1}{|c|}{Effective} & 37.5\% & 52.5\% & 25\% & 100\% \\ \hline
\end{tabular}
\end{table}

\begin{figure}[h]
  \includegraphics[width=\linewidth]{img/sample-coverage-graph-extended}
  \caption{Coverage of substitution candidates for sample program with monitors over constrained domain}
  \label{fig:sample-coverage-graph-extended}
\end{figure}
