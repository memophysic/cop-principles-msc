\section{Metrics}\label{sec:metrics}
\begin{figure}
  \includegraphics[width=\linewidth]{img/iso25023}
  \caption{Relationship among elements defined by the SQuaRE series of International Standards \cite{ISOIEC25023:2016}}
  \label{fig:iso25023}
\end{figure}

This section introduces two new metrics based on state space coverage for adaptive software: \textbf{\emph{modeled} and \emph{effective} adaptation space coverage}. Both are measures of state space coverage, but are based on different sources of reachable values. These metrics are described in a format inspired by ISO/IEC 25023. This standard is part of the Systems and software Quality Requirements and Evaluation (SQuaRE) series of International Standards. As shown in Fig. \ref{fig:iso25023}, various elements are defined in the context of SQuaRE, with ISO/IEC 25023 focusing on ``provid[ing] measures including associated measurement functions for the quality characteristics in the product quality model'' \cite{ISOIEC25023:2016}. To provide a complete picture of the scope of the proposed metrics, each element of the SQuaRE series is addressed directly. While modeled and effective adaptation coverage are different metrics, they are similar and therefore will be presented conjointly.

Because the focus of the current work is also on quality measurements, table \ref{tbl:adaptation-space-measures} was produced with the goal of presenting generic metrics. The base metric is the state space coverage, which is adequate for computing the coverage over the complete system. However, there exists an assumption for the adaptation space defined by adaptive components: any parameter not explicitly defined is considered entirely supported. In this case, the component is said ``agnostic'' to the parameter. From this, the primitive adaptation space coverage uses the number of possible states that a component is \emph{aware of}, that is only those originating from parameters explicitly defined in its adaptation space. For adaptive components with substitution candidates (``composite'' components), instead of computing the adaptation space by unionizing those of the candidates, we make use of the PASC to compute their respective coverage. That way, we can obtain the composite adaptation space coverage (CASC) simply by summing up the PASC of the candidates and subtracting the coverage related to the states defined multiple times by each candidate. This last metric is formalized as the recurrent state space coverage, applicable to a set of state spaces. With this scheme, it is possible to compute the coverage of any component within a composition hierarchy by requesting the primitive coverage of its substitution candidates. This is possible because all adaptive components are required to provide the adaptation space they support explicitly. Therefore, all substitution candidates are considered black boxes and each composition layer can be analyzed independently from each other.

In the remainder of this section, the elements defined in the SQuaRE series are addressed with a description for each of the metrics' context and properties.

\paragraph{Quality characteristic}
The quality characteristic being measured is the structural support for adaptation contexts of an application. We seek to determine if an application has the capability to support ranges of contexts it is expected to encounter.

\paragraph{Quality sub-characteristic}
When verifying component-based software, the structural support of contexts of an application relies on the support provided by each of its components. In adaptive software, the structure of components can change depending on the context. A quality sub-characteristic is therefore the structural support for adaptation contexts offered by components in a dynamic environment.

\paragraph{Measure definition}
The structural support of contexts will be measured using \emph{adaptation space coverage}, that is, state space coverage of components' adaptation space. It is defined as the proportion of the number of states supported by a component as defined in its adaptation space compared to the number of reachable states, either defined by parameters as a \emph{modeled space} or by monitors as an \emph{effective space}. This last distinction is reflected in variable B for the state space coverage function (see table \ref{tbl:adaptation-space-measures}): the ``number of possible states'' can be those provided by parameters or by their bound monitors. From this fact, the proposed coverage metrics are labeled ``modeled coverage'' when based on parameters and as ``effective coverage'' when based on monitors.
%Effective coverage can still be computed in a limited environment at design time if a PVP representative of the runtime monitoring group is provided.

\paragraph{Measurement functions}\label{par:measurement-functions}
The functions for modeled adaptation space coverage (MASC) and effective adaptation space coverage (EASC) are given in table \ref{tbl:measurement-functions}. A few definitions of terms are needed to understand the functions.

An adaptive component \(c\) has an adaptation space \(\sigma_c\) defined as a subspace of a context space where every contextual variable is a dimension. The states of each contextual variables are defined at design-time using parameters and at runtime by monitors. Therefore, two types of context spaces respectively exist: modeled context space \(\Omega\) and effective context space \(\omega\).
A composite component has \(n\) substitution candidates \(s\) with each an adaptation space \(\sigma_{s_i}\). When looking at a context space constrained to the parameters known by a set of components \(c_{0 \dots n}\), the modeled context space is \(\Omega_{c_{0 \dots n}}\) and the effective context space \(\omega_{c_{0 \dots n}}\).

The terms found in table \ref{tbl:measurement-functions} are summarized below.

\begin{itemize}
  \item \(\sigma_{s_i}\) : Adaptation space of substitution candidate \(s_i\) for a given component
  \item \(\Omega_{s_i}\)/\(\omega_{s_i}\) : modeled/effective context space of substitute candidates for a given component
  \item \(\Omega_{s_{0 \dots n}}\)/\(\omega_{s_{0 \dots n}}\) : modeled/effective context space of all substitute candidates for a given component
  \item \(c(x)\) : Number of states in the state space \(x\) (where \(x\) could be the state space corresponding to \(\sigma\), \(\Omega\) or \(\omega\))
  \item \(r(s_{0 \dots n})\) : Number of recurrent states for all substitute candidates (obtained with an algorithm)
\end{itemize}


%Requirements on these elements are \(\omega \subseteq \Omega_M\), \(\Omega_E \subseteq \Omega_M\). Also, unless manually redefined (e.g. to encapsulate the adaptation scheme), a composite component has for adaptation space: \(\omega_C = \bigcup{\omega_{s_{0 \dots n}}}\).



\begin{table}[h]
  \centering
  \caption{Measurement functions for metrics}
  \label{tbl:measurement-functions}
  \begin{tabulary}{\linewidth}{|l|Ll|}\hline
    Metric & Function & \\ \hline
    MASC &
    \(CASC(\) &
    \begin{tabular}[t]{l}
      \(A_i=PASC(A=c(\sigma_{s_i}), B=c(\Omega_{s_i}))\), \\
      \(B = RSC\left(
        A_j=r(\sigma_{s_{0 \dots n}}),
        B=c(\Omega_{s_{0 \dots n}})
      \right))\)
    \end{tabular}\\ \hline
    %\((\sum_{i=0}^{n}{\omega_{s_i}} - RSC(s_{0 \dots n})) / \Omega_M\)\\ \hline
    EASC &
    \(CASC(\) &
    \begin{tabular}[t]{l}
      \(A_i=PASC(A=c(\sigma_{s_i}), B=c(\omega_{s_i}))\), \\
      \(B = RSC\left(
        A_j=r(\sigma_{s_{0 \dots n}}),
        B=c(\omega_{s_{0 \dots n}})
      \right))\)
    \end{tabular}\\ \hline
  \end{tabulary}
\end{table}

% Table \ref{tbl:measurement-functions} shows the measurement functions for the two metrics.
We can see that the functions from table \ref{tbl:measurement-functions} are implemented using the generic measurement functions presented in table \ref{tbl:adaptation-space-measures}. More specifically, both MASC and EASC metrics use the \(CASC\) measurement function. Note that when using this measure on a \emph{primitive} component instead, the equation is equivalent to \(PASC\) since \(RSC = 0\) and there are no substitution candidates, therefore the only primitive for the sum of \(PASC\) is itself. Resulting values range from 0.0 to 1.0, where 1.0 is the ideal value. Values < 1.0 indicate that there are unsupported contextual states by the component.

\paragraph{Quality measure elements (QMEs)}
QMEs are the \emph{number of states} defined in state spaces of different artifacts.

\begin{itemize}
  \item \textbf{Number of modeled context states} as defined by parameters. They represent the reachable contexts as modeled at design time. Each parameter provides a dimension of the state space.
  \item \textbf{Number of effective context states} as defined by monitors. They represent the reachable contexts which can be encountered at runtime for a given set of monitors. Like parameters, each monitor provides a dimension of the state space.
  \item \textbf{Number of component adaptation states} as defined by an adaptive component. They represent the viability zone of the component, that is, the contexts it supports. The states are a subset of those defined by the parameters.
\end{itemize}

\paragraph{Measurement method}
The method used is the same for all QMEs: acquisition and inspection of state spaces from artifacts (parameters, monitors and adaptive components). At design time, the state space of the parameters the component is aware of can be statically acquired, rendering static analysis of modeled adaptation space coverage possible. At runtime, the state space of the monitors can be acquired by obtaining the list of monitors corresponding to the list of parameters the adaptive component is aware of (e.g. from a monitoring manager entity).

\paragraph{Property to quantify}
The property to quantify is the adaptation space coverage of an adaptive component. This quality property indicates what proportion of contexts that an application can encounter a certain component explicitly supports.

\paragraph{Target entities}
Adaptive components, parameters and monitors.

% The coverage ratio is computed rather trivially for a non-composite component. An adaptation space is formed by pairs of state space definitions \({\omega_P}_i\) and its parameter \(P_i\). The number of combinations of defined values for each parameter \(i\) forms the reachable state space for this component. At a system level, a similar computation can be done to obtain the number of reachable states for the parameters the component defines. The state spaces are in this case all the possible values of a parameter \(P\) (modeled) or a monitor \(M\) (effective): \({\Omega_{\{P,M\}}}_i\).
%
% The metric's computation for a non-composite ``primitive'' component is summarized in equation \ref{eq:coverage}.
%
% \begin{equation}
%   C_{primitive} = \frac{|{\omega_P}_i|}{|{\Omega_{\{P,M\}}}_i|}
%   \label{eq:coverage}
% \end{equation}
%
% \newcommand{\recurringcoverage}{\(C_{R{\gamma_{0 \dots N}}}\)}
% Unless manually specified, composite components have their state space defined as the union of the state spaces \(\gamma\) of the \(N\) components acting as substitution candidates. As previously mentioned, this leads to the specification of some supported states which were not explicitly defined in candidates when multiple parameters are involved. Composite adaptive components must be component differently, otherwise primitive computation of the coverage would include those additional unsupported states.
%
% A way to accurately compute this metric is therefore to compute the coverage \(C_k\) of individual substitution candidates \(k\), sum them and subtract the coverage related to the candidates' recurring state definitions \recurringcoverage. This subtraction is necessary because states defined in more than one component would otherwise be counted multiple times. This is summarized in equation \ref{eq:composite-coverage}.
%
% \begin{equation}
%   C_{composite} = \left( \sum_{k=0}^{N} {C_{primitive}}_{k} \right) - C_{R{\gamma_{0 \dots N}}}
%   \label{eq:composite-coverage}
% \end{equation}

% Typically, as the number of parameters and state spaces grows, such computations would quickly escalate as to reach the computational limit. To prevent this, implementations of state spaces can be devised to define these state spaces lazily and compute the variables needed by the metrics without the need to explicit every state or keep the in memory. A lazy implementation is planned for two of the three types of state spaces: discrete and continuous. Because enumerated state spaces are expected to be quite small (<< 100 elements) and each state expressed explicitly, the proposed approach was impractical.
%
% The remaining challenge is to compute the expression \recurringcoverage. Because we are required to find duplicates of states, one solution is to go through the defined states and count how many components support it: if it is more than 1, add to the duplication count. The numerator is then divided by a denominator representing the total number of possible states.
%
% It might also be possible to devise a more efficient solution by using abstract operations on state spaces, but is beyond the scope of the current work.
