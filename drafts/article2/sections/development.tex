\section{Development}\label{sec:development}
The computation of the metrics was implemented to demonstrate its application. Because the necessary artifacts were implemented as part of our previous work in the library AdaptivePy, it was reused and improved to support more state space operations necessary for the calculations. Originally, the library only supported enumerated state spaces. The new version adds the abstract handling of state spaces along with basic operations such as unions and intersections.

%State spaces replaced the previous dictionary-based implementation for specification of possible values in both parameters and monitors.
Using discrete state spaces, it is possible to efficiently define parameters which have a very large state space. One example is how temperature is represented for measurements ranging from -40\textdegree~to 60\textdegree~Celcius at the precision of 0.5\textdegree, expressed as [-40, 60, 0.5]. This leads to a state space of \(\frac{60-(-40)}{0.5}=200\) states. Additionally, a second parameter could be used to reinterpret these values in more human-readable terms such as \{cold, normal, hot\} by using thresholds within a monitor. Since monitors can define values subset of their parameter, precision could be lower (e.g., 1\textdegree) and limited to a smaller range (e.g., -30\textdegree to 30\textdegree).

The metrics’ computation is done through a single function which takes as input an adaptive component and returns a value in the domain [0, 1], representing a percentage between 0 and 100\%. The proposed solution in our previous work to simplify construction of complex adaptive component hierarchies was to assume complete support of state spaces defined by substitution candidates. This means that there is no need to query further than the direct substitution candidates to obtain a complete definition of the adaptive component's adaptation space. This assumption was used to automatically compute the default adaptation space of a composite adaptive component as the union of its substitution candidates'.

For the metrics' computation, this means that substitution candidates are considered primitive components because they are expected to support the complete adaptation space they provide. Any undefined states can be manually supported by parametrization within the composite adaptive component or by further constraining its adaptation space. This reduces the problem of state space size explosion by only requiring the computation of recurring coverage to a single set of primitive components for any composite adaptive component.

Computation of modeled coverage is done without the need to instantiate components by using statically defined possible values for both parameters and components. For effective coverage, there is a need to instantiate components because monitors are registered to one or many parameter value providers (PVP) and a component can be bound to only one PVP. This constraint was proposed in our previous work \cite{longchamps2017} to enable agreement with regards to context between collaborating components. An implication of this constraint is that depending on which PVP the component is bound to, different monitors can be set and cover equally different regions of the viability zone. Two sets of monitors can therefore provide different coverage values.

A sample program was written to demonstrate the use of the metrics. The program represents an adaptive caging system for a zoo which allows the zookeepers to distribute and transfer animals to appropriate cages. Two contextual variables are used to make adaptation decisions and are modeled as parameters:

\begin{itemize}
  \item \textbf{AnimalSize (AS)}: \{small, medium, large, gigantic\}
  \item \textbf{Temperature (T)}: [-40, 60, 0.5]
\end{itemize}

An \textbf{AnimalContainer} interfaces describes what is expected to be accomplished by the adaptive component: an animal can be set in the container, it can be checked for emptiness and it can be cleaned. An adaptive component using substitution therefore needs to adapt to the various animal sizes and temperatures. Three types of animal containers are defined as three adaptive components with the following adaptation spaces:

\begin{itemize}
  \item \textbf{Cage}: AS=\{small, medium\}, T=[10, 60, 0.5]
  \item \textbf{Barn}: AS=\{small, medium, large\}, T=[-40, 30, 0.5]
  \item \textbf{VirtualEcosystem (ES)}: AS=\{gigantic\}
\end{itemize}

Note that since VirtualEcosystem doesn't define any adaptation over the temperature parameter, it is agnostic to it, meaning it supports any temperature and isn't expected to adapt to temperature changes.
