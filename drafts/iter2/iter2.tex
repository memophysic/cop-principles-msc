\title{Design pattern for addition of adaptive behavior in component-based software design}
\author{Samuel Longchamps (\url{samuel.longchamps@usherbrooke.ca})}

\documentclass[10pt,twocolumn]{article}
\usepackage{../itersummary}
\addbibresource{../COP-MSC.bib}

\begin{document}\twocolumn\maketitle
\section{Summary of iteration 1}
Iteration 1 provided an overview on the problems faced by developers and engineers when developing adaptive applications. Toolkits are de facto means of providing reusable components for designing and implementing graphical user interfaces. Complementary adaptation techniques were formalized as parametrization and substitution to easily achieve adaptation in an iterative development process.\\

The \emph{Monitors} design pattern proposed is a first solution to encapsulate an adaptation concern: contextual information translation logic between adaptation data and components depending upon them. Two types of monitors, static and dynamic, provide a mean to translate adaptation data provided by some sensor to values within an explicitly defined context domain. Static monitors require external scheduling to acquire a value for an adaptation parameter while dynamic monitors are observable entities (observer pattern) to which components can subscribe and be alerted of an updated value, for example when the value is different from the previously acquired one. A static monitor can be promoted to a dynamic monitor by means of a dynamic monitor decorator, allowing optimal reuse of data acquisition logic and scheduling as composite parts.\\

Adaptive components are components which exhibit adaptation behavior through parametrization and/or substitution. Our requirement for a component to be adaptive is for it to \emph{provide a mean to obtain which adaptation parameter and related domain it is defined for}. For this domain, the component is assumed to be aware of the corresponding adaptation parameters, acquire monitors corresponding to them and adapt themselves based on the value they provide. The same information provided by the monitor can be used to implement adaptation through component substitution. The adaptation execution control can be propagated both in a top-down and bottom-up manner through either request of update to lower components or notification, respectively.

\section{Approaches to software adaptation design}
\subsection{Variability modeling}
The approach proposed by \citet{Berkane2015} has similar goals to the current research project and provides important contributions toward technology-independent tools to assist developers and engineers in designing and implementing adaptive software with a clear separation of concerns. A model linking primitive (Gang of Four) and advanced (composite) design patterns based on the work of \cite{ramirez2010design} is used to show implementations at different abstraction layers: functional, logical and technical. The functional layer is the highest level and is in fact IBM's MAPE-K model (Monitor, Analyze, Plan and Execute all sharing a common Knowledge). The model allows to explicitly define feedback control loops (FCL) in the design. Their implementation is done through various patterns in a flexible way, that is not all patterns necessarily need to be used. Such model is a good example of a tool for developer to visualize and plan which pattern from a repository will be useful for each steps of the adaptation process. Components presented in their model exhibit the important property of being reusable as they address specific concerns, which is exactly our goal. There is however an absence of technique or guidelines assisting developers in applying these patterns iteratively and partially, rendering hard to predict both cost and gains of their implementation in an existing software.

\section{Adaptation primitives}
\subsection{Monitoring group}
A monitoring group is a dictionary-like construct where one adaptation parameter has a one-to-one relation with at most one monitor. A monitoring group is used to declare explicitly the complementary state of a set of monitors such that their aggregation forms the local view over a context. The group of adaptation parameters which are monitored by the set of monitors in a monitoring group then represents the specific adaptation space of the local context.

\section{Formalized adaptive components}
\label{sec:formalized-adaptive-components}
\subsection{Principle}
An adaptive component can adapt its behavior (parametrization) or identity (substitution) based on values of certain adaptation parameter. To ensure a consistency and be able to verify the adequacy of a software's state, adaptive components are required to provide the domain over which they are defined, referred to as adaptation space. Should the context of the system violate the context over which the component is defined, the system can take actions to remedy the situation, for example by choosing more adequate components as a substitute or simply deactivate the features depending on the affected components. Many existing strategies have been proposed to accomplish this feature, such as having a constraint evaluator frequently checking policies and notifying an adaptation engine to perform necessary adaptations upon policy violation \cite{garlan2004rainbow}.\\

In a decentralized environment, such central entity wouldn't be adequate and the constraints would need to be distributed among affected components. The constraint evaluation scheduling would also need to be managed and controlled, preventing two components to use scheduling which is specific to their use in a decoupled way. However, a completely decoupled process could give rise to performance issues by not taking into account the application-wide demand for adaptation parameters and superfluously duplicate data acquisition actions. Adaptive components have a requirement toward monitors which is summarized in statement \ref{eq:share-monitor-group}:

\texteq{Components which are part of the same context must share the same monitoring group.}{eq:share-monitor-group}

The reason for this requirement is that we cannot assure consistency if one adaptation parameter evaluates to a context state for one component and, in parallel, to another for a second component which has a dependency relation to the first. A metaphor for this requirement is the incoherent situation where a clock would display 2PM and 3PM at the exact same moment: how could one know if he is in advance or late for a meeting taking place at 2:30PM? For a second person in a different context, his time zone could be used to translate to a reference one (the one used for the meeting) and then a decision could be made (which is an example of a second local context). Most importantly, no sharing of monitoring value could be possible between components part of the same context which would bring out the performance problem specified earlier.\\

We propose a solution to this problem in the form of a simple adaptive component design pattern which uses monitors obtained through a concrete realization of a monitoring group. This component is simply called \emph{monitors managers} and rather than being application-centric (global), it is context-centric (specific to local context). This monitors manager is provided to lower components by the topmost adaptive component in the form of a monitors provider which forbids any component form making change to the monitors configuration.

\subsection{Design pattern}
\begin{figure}
  \includegraphics[width=\linewidth]{adaptive_component}
  \label{fig:adaptive-component-uml}
  \caption{UML diagram of adaptive component}
\end{figure}

The pattern is shown in figure \ref{fig:adaptive-component-uml}. The basic component contract is enforced by the \emph{Adaptive} interface which provides the defined context and explicitly defines a single monitors provider which should remain immutable unless indicated otherwise in the implementing classes. The basic building blocks of parametrization and substitution are, respectively, the abstract classes AdaptiveSubstitute and AdaptiveProxyRouter. An AdaptiveSubstitute gets its MonitorsProvider by dependency injection. This feature is automated in the AdaptiveProxyRouter where calls to the instance method \verb|monitors_provider()| are forwarded to the class method \verb|composite_monitors_provider()|. Class implementing AdaptiveSubstitute can use the \verb|monitors_provider()| method to acquire necessary monitors for the parameters which are part of their defined context. This is not enforced, it is a contract and it is possible for the MonitorsProvider instance to be unable to provide a monitor for a given parameter. This is further discussed in \ref{sec:adaptive-component-usage}.\\

The AdaptiveProxyRouter component is responsible to manage substitution of functionally equivalent components (substitutes). Two main methods are used for this management: \verb|choose_substitute()| and \verb|route_to_substitute|. The following describes in more details the concerns these methods relate to:

\paragraph{substitution\_candidates()}
This method is responsible for choosing among the substitution candidates (provided by the \verb|substitution_candidates()| method). In a first implementation, this concern can be addressed directly in the implementing class and further extracted to separate the concern from the business logic. This becomes useful especially when a more complex strategy such as constraint solving is used with policies which is a reusable strategy.

\paragraph{route\_to\_substitute()}
This method is responsible for routing the proxy component's appropriate methods to the chosen substitute's. The way this routing is performed varies greatly depending on the programming language and framework used. For example, in Python, one can simply recover all methods from the common interface implemented and assign the method passing the substitute as \verb|self|. This implementation is most beneficial since this generic code can be included directly in the AdaptiveProxyRouter abstract class.\\

The monitors manager can be added to the architecture in a separate refactoring step of an application with global monitors provided either by a central monitors manager or through dependency injection in components. Because an application with an originally low level of plasticity depended on few adaptation parameters, the cost of implementing a managing entity for all monitors could've been too high for little gain. The same is true when monitors are statically defined and configured. Adaptive components are only required to provide the domain of adaptation they are defined to work with which doesn't impose a requirement on the monitors manager module. Nevertheless, it is included in the pattern because its use from the beginning can be beneficial in term of separation of concerns and forces developers to comply to the context-local monitoring group requirement explicitly. This reduces risks of implementing this pattern incorrectly and introducing inconsistencies in the adaptation mechanisms.

\subsection{Usage}\label{sec:adaptive-component-usage}

\begin{figure}
  \includegraphics[width=\linewidth]{adaptive_component_example}
  \label{fig:adaptive-component-example-uml}
  \caption{UML diagram of an example implementation for the adaptive component design pattern}
\end{figure}

Figure \ref{fig:adaptive-component-example-uml} presents a typical example of the application of this pattern. The interface \emph{CommonInterface} is used to represent a domain-specific component which is used in a given software to model the business logic. Substitute components \emph{Substitute[A, B, C]} each define a context domain for which they are defined. The \emph{ProxyComponent} inherits in its adaptation space an equivalent degree of parametrization to the set formed by the defined context spaces of its substitute candidates. Concretely, this means that if for example Substitute A, B and C are each defined solely for one type of weather respectively such as sunny, raining and snowing in term of parametrization, the proxy component has for parametric adaptation space all three weather states. For a parent component, the substitutes are hidden and only its adaptation space is known.\\

Scheduling of adaptation relies entirely on Monitors. This approach is different from other well-known approaches where one or many separate and dedicated entities are responsible for deciding when to apply adaptation, for example by monitoring certain metrics and alerting an adaptation engine of a value exceeding a certain threshold. Monitors are assembled hierarchically in such a way that abstraction layers from concrete sensor data to domain-specific states can be designed and adapted for the needs of an application. The gain is that components for a given application will define eloquently their adaptation space in term of the domain. Concerns such as data acquisition (sensors), filtering, formatting and aggregation can all be realized in a dedicated monitor. Components can then expect that when an adaptation parameter value it relies on changes, it always need to react to it (otherwise a filtering monitor can be added to the monitor provided by the monitors provider). 

\printbibliography

\end{document}
