\documentclass[10pt,conference,compsocconf]{IEEEtran}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{graphicx}
\usepackage{times}
\usepackage[bookmarks={false},colorlinks={false}]{hyperref}
\usepackage[backend=biber, style=numeric]{biblatex}
\newcommand{\citet}[1]{\citeauthor{#1} \cite{#1}}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage[]{algorithm2e}
\usepackage{caption}
\captionsetup{font=footnotesize,justification=centering,labelsep=period}

\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map[overwrite=true]{
      \step[fieldset=doi, null]
      \step[fieldset=url, null]
      \step[fieldset=isbn, null]
    }
  }
}

\addbibresource{../../COP-MSC.bib}
\addbibresource{../gei705.bib}

\begin{document}

\title{GEI-705 - Développement de composants adaptatifs}
\author{\IEEEauthorblockN{\large Samuel Longchamps}\IEEEauthorblockA{Prototype 3 - Application avec composants adaptatifs}}

\markboth{\today}{}

\maketitle

\emph{Le code de l'application dont il est question dans ce document est accessible à l'adresse :\\ \url{https://gitlab.com/memophysic/gei705-adaptive/tree/master/gei705/prototype3}}

\section{Introduction}
Dans une application graphique, l'architecture de base qui est utilisée est basée sur une modélisation par composition hiérarchique de composants. Une caractéristique des composants qui est cependant absente est la capacité de facilement remplacer un composant par un autre qui partage la même interface. En fonction du service qu'offre un composant, ce dernier peut être plus efficace ou fiable en fonction de certains facteurs qu'un autre. Ainsi, l'adaptation par substitution a pour but de faire une sélection d'un composant adéquat en fonction de la maximisation ou la minimisation de ces facteurs. Un exemple d'utilisation de cette branche de l'adaptation est dans les systèmes réseautés qui doivent garantir une certaine qualité de service, soit donc que le temps de réponse ne puisse dépasser une certaine valeur ou que le nombre de requêtes traitées ne peut passer sous un certain barème.

Lorsqu'il s'agit d'interface graphique, les facteurs liés aux choix d'adaptation sont plutôt orientés vers les utilisateurs de l'application. Des modèles de groupes d'utilisateurs et de types de tâches (\emph{workflow}) sont développés et le défi est alors de faciliter la convivialité du système en liant les types d'interfaces qui se prêtent à certaines tâches et certains utilisateurs. Cette modélisation peut être faite de plusieurs façons (manuellement, par l'analyse de données récoltées lors de sessions de tests, par algorithmes d'apprentissage d'intelligence artificielle, etc.). Le présent prototype a pour but d'utiliser l'approche développée au prototype 1 avec des chaînes de Markov comme engin d'inférence qui permettra de sélectionner un composant pour une tâche en fonction de facteurs modélisés. Les chaînes de Markov permettent d'émettre une prédiction de l'utilisation future à partir d'une accumulation statistique de l'utilisation. En modélisant manuellement les éléments caractéristiques d'une tâche d'édition, il sera possible d'identifier statistiquement les tâches réalisées par l'utilisateur et de sélectionner les composants qui sont le plus propices à ces types de tâches.

\section{Description de l'application}
L'application réalisée est un simple livret d'adresses informatisé qui permet de consulter le nom, le numéro de téléphone, l'adresse postale et de courriel d'une personne. Le livret permet uniquement la modification des champs cités précédemment et la tâche est donc la modification de ces informations. Le livret peut être vu comme un dictionnaire où le nom d'une personne est une clé et les données un autre dictionnaire avec le titre du champ comme clé.

Deux caractéristiques ont été modélisées pour une telle tâche :

\begin{enumerate}
  \item Entrées : Modification unique ou de plusieurs entrées
  \item Champs : Modification unique ou de plusieurs champs
\end{enumerate}

De ces deux caractéristiques, différents types d'interfaces peuvent être envisagées. Par exemple, dans le cas où la tâche est de modifier plusieurs entrées et plusieurs champs, une vue sous forme de grille offrira une vue de tous les champs nécessaires. Cependant, si la tâche consiste à modifier les champs d'une seule entrée, la surcharge de l'affichage peut être réduite en utilisant une vue sous forme de formulaire, affichant une seule entrée à la fois. Ces deux vues sont présentées à la figure \ref{fig:views}. L'idée ici est de constater que ceux deux approches sont en réalité des composants distincts qui supportent la même interface ``modification d'information de contact'' et dont le choix de l'un ou l'autre peut être donné en fonction des caractéristiques citées précédemment. Les chaînes de Markov peuvent alors être utilisées pour accumuler les interactions de l'utilisateur et produire une prédiction du composant le plus approprié en vue de la tâche à réaliser.

\begin{figure}
  \includegraphics[width=\linewidth]{views}
  \caption{Vues de l'application. \emph{Gauche.} Vue formulaire \emph{Droite.} Vue grille}
  \label{fig:views}
\end{figure}

\section{Composant adaptatif}
Dans le prototype, un composant adaptatif a été développé grâce à la librairie ``AdaptivePy''. Ce dernier contient les opérations nécessaires à la substitution d'un composant par un autre et à la sélection du composant en fonction des valeurs contextuelles relevées. Ces valeurs contextuelles sont fournies par deux types de moniteurs : un type contrôlable et un autre type adaptatif contenant une chaîne de Markov. Il est ainsi possible de fixer les valeurs contextuelles à des valeurs données pour pousser le composant adaptatif à s'adapter à une situation manuellement. En mode ``automatique'', la chaîne de Markov est utilisée pour produire les valeurs et l'adaptation est donc basée sur cette méthode. Pour conserver une bonne séparation des préoccupations, un modèle du carnet d'adresses est utilisé pour effectuer les opérations de modification. Ce modèle est fourni au composant adaptatif qui les injecte à son tour dans les composants qui constituent les candidats de substitution. Un composant adaptatif peut ainsi être réutilisé pour effectuer l'édition de différents livrets d'adresses sans nécessiter de modification.

\section{Déroulement de l'adaptation}
Au début de l'application, une simple fenêtre avec un bouton permettant d'ouvrir l'éditeur du livret de contact est présentée. En cliquant dessus, l'éditeur se lance. Cette approche permet de diviser en sessions distinctes les interactions de l'utilisateur et donc de permettre l'accumulation de données d'interaction par le biais d'un modèle de livret avec la fonctionnalité de journalisation des mises à jour. Une fois la fenêtre de l'éditeur fermée, le journal est analysé et nous permet de déterminer les valeurs des paramètres de multiplicité des entrées et des champs par entrées modifiés. Au lancement de l'éditeur subséquent, l'adaptation aura lieu en fonction de la prédiction offerte par les chaînes de Markov de la valeur de multiplicité des deux caractéristiques citées précédemment.

Plus simplement, cela permettra d'afficher une vue attendue plus appropriée en fonction des interactions prédécentes. Cette approche est limitée par l'absence de modélisation d'autres tâches reliées pour grouper les sessions en groupe de tâches, mais l'implémentation montre que l'approche est applicable relativement facilement. En plus, l'utilisation d'AdaptivePy rend la tâche de créer le composant adaptatif fiable et facilement extensible. Par exemple, si une troisième vue était implémentée pour effectuer l'édition d'un seul champ pour plusieurs entrées (ex., changement de l'adresse de domicile de plusieurs personnes), il serait facile d'ajouter ce candidat pour les valeurs de paramètres ``Entrées: Multiple; Champs: Simple''.

\section{Discussion et conclusion}
L'approche présentée dans ce document est l'application de chaînes de Markov comme engin d'inférence visant l'adaptation de composants dans une interface graphique. Un composant adaptatif permet la substitution de différents composants remplissant la même fonction, mais organisant l'information d'une façon différente. Pendant l'exécution de l'application, diverses sessions d'utilisation permettent de récupérer des informations d'interactions qui sont utilisées pour déterminer le type d'utilisation - la tâche - que l'utilisateur réalise le plus. Les chaînes de Markov permettent alors de fournir une prédiction sur la future tâche en fonction d'une modélisation de l'utilisation passée. Cette approche permet alors d'automatiser la sélection d'une vue la plus appropriée pour l'utilisateur. Comme le prototype n'est pas une application complète, les données d'interactions sont peu nombreuses, mais un logiciel complet pourrait utiliser un nombre arbitraire de chaînes de Markov pour différents types d'interactions. Il est même possible d'utiliser la combinaison de plusieurs interactions en groupe afin de modéliser une tâche plus haut niveau. La prédiction de cette chaîne pourrait ensuite être utilisée pour effectuer la prédiction d'une vue appropriée. Un algorithme d'intelligence artificielle à plusieurs niveaux serait alors un outil pertinent pour le développement d'une telle approche.

En conclusion, le prototype développé démontre que les chaînes de Markov constituent un type d'outil pouvant servir de déclencheur pour l'adaptation dans une application graphique. Aussi, le prototype démontre que les architectures hiérarchiques de composants tels que ceux présents dans les interfaces graphiques constituent une cible viable pour l'adaptation. Il est possible de séparer les préoccupations d'une façon adéquate avec les bons patrons de conception (tels que ceux implémentés dans la librairie AdaptivePy) et de produire une application fiable et prédictible. L'utilisation de plusieurs groupes de moniteurs est nouvelle et permet de donner à l'utilisateur un contrôle sur le niveau d'adaptativité de l'application.

% \printbibliography

\end{document}
