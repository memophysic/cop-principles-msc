\documentclass[10pt,conference,compsocconf]{IEEEtran}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{graphicx}
\usepackage{times}
\usepackage[bookmarks={false},colorlinks={false}]{hyperref}
\usepackage[backend=biber, style=numeric]{biblatex}
\newcommand{\citet}[1]{\citeauthor{#1} \cite{#1}}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage[]{algorithm2e}
\usepackage{caption}
\captionsetup{font=footnotesize,justification=centering,labelsep=period}

\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map[overwrite=true]{
      \step[fieldset=doi, null]
      \step[fieldset=url, null]
      \step[fieldset=isbn, null]
    }
  }
}

\addbibresource{../../COP-MSC.bib}
\addbibresource{../gei705.bib}

\begin{document}

\title{GEI-705 - Développement de composants adaptatifs}
\author{\IEEEauthorblockN{\large Samuel Longchamps}\IEEEauthorblockA{Prototype 2 - Architecture hiérarchique de composants}}

\markboth{\today}{}

\maketitle

\emph{Le code de la librairie dont il est question dans ce document est accessible à l'adresse :\\ \url{https://gitlab.com/memophysic/gei705-adaptive/tree/develop/gei705/prototype2}}

\section{Introduction}
La programmation orientée composant est une approche visant à modulariser un programme informatique en organisant des parties logicielles de sorte qu’il soit facile d’assembler des modules haut niveau en une application complète. Un composant est la plus petite unité permettant d'être assemblée. En développant des composants qui englobent une responsabilité unique et encapsulent sous forme de boîte noire les détails techniques d'une implémentation, ces derniers mènent à un code qui est beaucoup plus lisible et testable.

Typiquement, une architecture de composants peut être modélisée sous forme de graphe orienté où les noeuds sont les composants eux-mêmes et les liens des relations de dépendances entre les composants. Un composant peut nécessiter un service d'un autre composant ou offrir un service à un autre client. Parfois, des connecteurs sont utilisés pour connecter deux composants. Ces connecteurs servent à découpler entre elles les relations de dépendance pour que deux composants ne connaissent qu'une interface de service plutôt que le composant concret l'offrant.

Dans les interfaces graphiques, l'architecture employée pour les différents composants est celle d'un arbre. On parle ainsi de structure en couche stricte\cite{Szyperski2002}: un élément de l'arbre peut uniquement dépendre des fonctionnalités offertes par les éléments directement dessous, ici ses enfants. Les dépendances entre les composants sont orientées vers les enfants; un noeud requiert les services de ses enfants. Dans une application réelle, les différents composants offrent ensemble une certaine fonctionnalité à l'utilisateur (sélection de données, affichage d'information, manipulation de données d'un modèle, etc.). Le but du présent travail est de fournir dans une librairie Python les artéfacts nécessaires à la construction d'une architecture par composant qui est hiérarchique et donc compatible avec le modèle employé dans les interfaces graphiques.

\section{Artéfacts}\label{sec:artefacts}
Dans un arbre, deux types d'éléments existent : les noeuds et les feuilles. Un noeud peut être le parent d'un ou plusieurs autres éléments, tandis qu'une feuille ne peut pas. Les deux ne peuvent avoir qu'un seul parent. Le seul élément à ne pas avoir de parent est celui à la racine de l'arbre. Si l'on fait la traduction dans le contexte des composants, on pourrait parler de composant ``simple'' pour une feuille et un composant ``composite'' pour un noeud. Un composant composite peut se voir appliquer différentes contraintes sur sa relation envers ses enfants. Par exemple, les enfants pourraient être limités à des instances uniques (pas de doublons), ou bien un ordre des enfants pourrait être important dans le cas de doublons. Comme un service pourrait être fourni par plusieurs composants, un ordre de composants compatibles permettrait de mettre en place des patrons de qualité de service pour répartir les requêtes entre les enfants ou l'envoyer à l'enfant jugé le plus efficace en fonction de la requête. Ces contraintes peuvent être modélisées par des types de ``conteneurs de composants'' où la structure de donnée utilisée implémente la contrainte. Dans le cas des exemples précédents, l’ordre peut être fourni par une liste et l'unicité par un ensemble.

De plus, pour assurer la validité de l'architecture, certaines contraintes existent sur les relations entre les éléments. Ces dernières sont :

\begin{itemize}
  \item Un noeud ne peut pas avoir un ancêtre\footnote{Les ancêtres sont l'ensemble du parent, grand-parent, grand-grand-parent, etc.} comme enfant. Cette condition est nécessaire pour ne pas qu'il y ait de cycle dans le graphe et donc assurer que ce dernier représente bien un arbre. Cela impliquerait au niveau de l'implémentation qu'il y aurait un cycle de dépendance qui rendrait impossible la compilation/l'interprétation du code.
  \item Un noeud ne peut être son propre enfant. Cette condition prévient une impossibilité similaire au point précédent.
\end{itemize}

\section{Développements}
\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{lib-uml}
  \caption{Schéma UML de la librairie d'architecture hiérarchique pour composants}
  \label{fig:lib-uml}
\end{figure}

La librairie implémentant l'architecture hiérarchique propose deux interfaces génériques pour un composant simple ``Component'' et un composite ``CompositeComponent''. Une interface ``ComponentsContainer'' offre les fonctionnalités de base d'ajouter, enlever et retrouver des composants suivant les contraintes expliquées à la section \ref{sec:artefacts}. Le schéma UML de la librairie est présenté à la figure \ref{fig:lib-uml}. On peut y voir qu'une implémentation de base de chacune des interfaces de composant est fournie. ``BaseComponent'' contient une relation de composition optionnelle vers un ``CompositeComponent'' qui est un parent. Un parent doit obligatoirement être un composite et non une feuille, sinon il serait impossible d'avoir une architecture de plus d'un composant. ``BaseCompositeComponent'' a une relation d'agrégation vers un nombre indéfini de ``Component'' qui sont considérés comme ses enfants. En plus, une relation de composition vers une implémentation de ``ComponentsContainer'' est ajoutée de sorte que cette instance sera utilisée comme délégué pour les opérations relatives à l'ajout et suppression des enfants. L'instance concrète est passée par injection de dépendance dans le constructeur de ``BaseCompositeContainer''. La responsabilité d'assurer la validité des relations avec les enfants et les parents (contraintes énoncées à la section \ref{sec:artefacts}) est donnée à ``CompositeComponent'' de sorte que les types implémentant ``ComponentsContainer'' puissent uniquement se concentrer sur la gestion au niveau des structures de données des composants.

\section{Discussion et conclusion}
L'approche présentée est relativement simple, mais illustre bien la façon dont une architecture par composants peut être produite avec une approche hiérarchique plutôt que par graphe plus général. Les assomptions et algorithmes liés aux arbres peuvent alors être utilisés pour optimiser les opérations liées à la manipulation des composants dans l'architecture, tant et aussi longtemps que les contraintes de dépendances sont respectées. Par exemple, il serait intéressant d'utiliser cette approche pour déduire les relations de dépendances optimales en fonction de certaines contraintes (ex.: exclusions mutuelles de groupes de composants, maximisation de la réutilisation de composition en fonction de ressources ou disponibilité d'unité computationnelle).

Dans le cas des interfaces graphiques, il serait intéressant d'assister les concepteurs d'interfaces graphiques en utilisant cette modélisation pour proposer des groupes de composants (représentés par un ``widget'') qui sont déjà utilisés dans l'application pour remplir une dépendance (ex.: un type de contrôle graphique). Cette approche pourra aider les développeurs à produire une interface graphique qui est plus uniforme en fonction de l'utilisation. Ceci serait un avantage pour l'utilisateur dans le cas où des opérations sont semblables, par exemple dans la sélection ou l'affichage de données. De plus, il serait possible d'utiliser une approche similaire à l'apprentissage par chaînes de Markov pour modéliser les séquences d'utilisation des branches pour déduire un indicateur lié à un groupe de composants et ainsi déterminer ceux qui sont les plus utilisés et donc les plus intuitifs pour les utilisateurs. Une interface graphique pourrait ainsi proposer des vues qui sont dont les choix de composants sont axés sur les données d'un groupe d'utilisateur, d'un environnement ou de toute autre variable contextuelle. La capture de ces données contextuelles serait donc nécessaire et agirait à titre de métadonnées utiles à la classification des observations. L'idée est alors de filtrer par domaine de valeurs des variables contextuelles les observations et produire les indices de prédictions des chaînes de Markov pour produire la vue. Concrètement, cela permettrait de générer en partie une interface graphique adaptée à un ensemble de domaines de valeurs contextuelles de façon automatique, assumant bien sûr que la récolte des observations a été également faite de façon automatique via d'autres logiciels partageant les éléments graphiques ou lors de tests de l'interface graphique brouillonne produite initialement par les concepteurs.

\printbibliography

\end{document}
