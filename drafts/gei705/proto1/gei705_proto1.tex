\documentclass[10pt,conference,compsocconf]{IEEEtran}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{graphicx}
\usepackage{times}
\usepackage[bookmarks={false},colorlinks={false}]{hyperref}
\usepackage[backend=biber, style=numeric]{biblatex}
\newcommand{\citet}[1]{\citeauthor{#1} \cite{#1}}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage[]{algorithm2e}
\usepackage{caption}
\captionsetup{font=footnotesize,justification=centering,labelsep=period}

\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map[overwrite=true]{
      \step[fieldset=doi, null]
      \step[fieldset=url, null]
      \step[fieldset=isbn, null]
    }
  }
}

\addbibresource{../../COP-MSC.bib}
\addbibresource{../gei705.bib}

\begin{document}

\title{GEI-705 - Développement de composants adaptatifs}
\author{\IEEEauthorblockN{\large Samuel Longchamps}\IEEEauthorblockA{Prototype 1 - Cas des interfaces usagers}}

\markboth{\today}{}

\maketitle

\section{Introduction}
L'adaptation dans les logiciels est une façon d'atteindre des requis basée sur un environnement changeant. Une approche typique pour l'implémentation de tels mécanismes est la modélisation de l'environnement, l'utilisateur et les interactions entre ces derniers. Les informations d'interaction récoltées peuvent être utilisées pour construire de tels modèles qui seront spécifiques à chaque utilisateur et à chaque contexte d'utilisation d'une application. Dans d'autres cas, un modèle général est construit par le biais de séances d'essais, par exemple avec des testeurs, pour modéliser leur utilisation du logiciel. Ainsi, un modèle préétabli peut être livré avec le logiciel et servira à faire des recommandations aux nouveaux utilisateurs.

Comme l'interaction avec les systèmes logiciels passe principalement par leur interface graphique, il va de soi que l'adaptivité peut être appliquée dans ce domaine. En effet, plusieurs façons de faire établies pour développer des contrôles d'interface adaptative existent (ex.: liste de raccourcis souvent utilisés, illumination ou assombrissement d'éléments, vues spécialisées pour les experts, etc.). Cependant, outre les contrôles, divers composants d'une interface graphique peuvent être adaptatifs.

Un mécanisme d'adaptation largement utilisé pour améliorer l'expérience utilisateur d'un logiciel est la prédiction des actions. En récoltant les informations d'interaction, il devient possible d'évaluer statistiquement la probabilité de la sélection suivante de l'utilisateur. Différentes approches existent pour mettre en place ce mécanisme. Un exemple est les \emph{chaînes de Markov} qui sont utilisées pour prédire un état suivant en se basant uniquement sur l'état courant. Assumant un espace d'états fini, l'idée est d'utiliser une matrice de probabilité des transitions qui est mise à jour au fur et à mesure que les interactions sont compilées dans le système. Une fois cette matrice construite, il est alors possible de l'utiliser pour déduire l'interaction qui mènera à l'état suivant avec la plus grande probabilité. Dans le cas des interfaces utilisateurs, il est possible de modéliser ces interactions sous forme de \emph{tâches} ou d'actions de l'utilisateur.

Un prototype a démonstratif a été développé qui démontre l'usage de chaînes de Markov pour construire la matrice de prédictions en temps réel. Le projet est accessible à l'adresse suivante : \url{https://gitlab.com/memophysic/gei705-adaptive}. Ce rapport présente les principes ayant servi à l'implémentation du prototype et propose des pistes de solutions pour généraliser leur utilisation dans une structure plus portable. Il va donc de soi que le présent prototype a une approche \emph{ad hoc} de l'implémentation et n'est qu'exploratoire.

\section{Chaîne de Markov}\label{sec:markov-chain}
\begin{figure}
  \centering
  \includegraphics[width=0.6\linewidth]{Markovkate_01.pdf}
  \caption{Chaîne de Markov à deux états\protect\footnote{By Joxemai4 - Own work, CC BY-SA 3.0, \url{https://commons.wikimedia.org/w/index.php?curid=10284158}}}
  \label{fig:markov-chain-two-states}
\end{figure}

Les chaînes de Markov sont nommées ainsi en l'honneur de leur inventeur Andrey Markov. Elles ont été utilisées en informatique notamment pour des algorithmes de classification de pages web comme PageRank de Google \cite{page2001method}. Une chaîne de Markov est un processus stochastique qui satisfait la propriété de Markov, soit que la distribution de probabilité conditionnelle des états futurs dépende uniquement de l'état courant et non des états précédents. Comme il est nécessaire de prédire un état futur avec une probabilité, il est tout de même nécessaire d'utiliser des informations acquises dans le passé. Ce requis est réconcilié avec les chaînes de Markov par la création d'un modèle des probabilités de transition qui est accessible (et mis à jour) dans l'état présent et ne garde pas en mémoire explicitement les interactions passées, mais plutôt une valeur de probabilité déduite des ces interactions.

Cette approche est notamment utilisée par \citet{bezold2011adaptive} pour construire un modèle statistique de l'utilisation d'un logiciel pour un utilisateur donné. Leur algorithme de prédiction est donné à l'équation \ref{eq:bezold-prediction}. ``Le résultant \(s(t)\) est la probabilité de la prochaine action de l'utilisateur. \(d\) est un facteur d'amortissement visant à donner un plus grand poid aux observations récentes, \(h\) est un vecteur contenant les interactions passées d'une longueur \(N\) et A est une matrice des probabilités de transitions des actions de l'utilisateur'' \cite{bezold2011adaptive}.

\begin{equation}
  \label{eq:bezold-prediction}
  s(t) = \sum_{j=1}^N d(j)h(t-j)A^j
\end{equation}

La matrice \(A\) est donc une modélisation instantanée de la probabilité des transitions qui est mise à jour à chaque interaction. Comme il n'est pas explicitement nécessaire de garder en mémoire les interactions précédentes, mais seulement d'appliquer une opération à chaque nouvelle interaction sur cette matrice, cette approche satisfait la propriété de Markov. Cette approche est celle utilisée dans le prototype démonstratif.

\section{Prédictions des actions dans une interface usager}\label{sec:user-interface-predictions}
Comme décrit à la section \ref{sec:markov-chain}, les interactions de l'utilisateur avec un logiciel peuvent être modélisées par des tâches ou actions effectuées en séquence. Ces actions, spécifiques au domaine d'utilisation visé par le logiciel, sont souvent utilisées dans une séquence particulière pour obtenir un certain résultat. Par exemple, il est possible d'obtenir un rendu de grains de bois en appliquant certaines opérations en séquence dans le logiciel de manipulation d'images ``Gimp''. Ces actions sont les suivantes:

\begin{enumerate}
  \item Fill color (Color=\#9d814f)
  \item HSV Noise (Holdness=2, Hue=0, Saturation=0, Value=255)
  \item Motion blur (Length=12, Angle=0)
\end{enumerate}

Dans le cas d'un infographiste, l'utilisation répétée de cette suite d'actions pourrait être détectée et la probabilité de l'action suivante à HSV Noise d'être Motion blur s'avérerait élevée. L'expérience de l'utilisateur pourrait alors être améliorée si on mettait à sa disposition, par exemple, un raccourci vers la prochaine étape, une liste de ces étapes en séquence ayant la plus grande probabilité ou simplement en lui indiquant l'emplacement du bouton de la prochaine opération à effectuer. Cette suite d'actions serait alors considérée comme une séquence \cite{bezold2011adaptive}. Il serait également possible de détecter si les présentes actions font partie d'une certaine séquence et modifier les prédictions en fonction du contexte actuel de l'utilisation, notamment en spécifiant un certain but. Dans l'exemple précédent, il s'agirait alors de détecter que l'utilisateur a comme but de produire un rendu de grains de bois.

\section{Description du prototype}
\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{markov_chain_demo.png}
  \caption{Prototype démonstratif des chaînes de Markov}
  \label{fig:markov-chain-demo}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{markov_chain_demo_case.png}
  \caption{Séquence d'actions et prédictions par chaîne de Markov}
  \label{fig:markov-chain-demo-case}
\end{figure}

Le prototype développé implémente une chaîne de Markov telle que présentée à la section \ref{sec:markov-chain} et utilise une variation de l'équation \ref{eq:bezold-prediction} comme modèle de prédiction. Le prototype, présenté à la figure \ref{fig:markov-chain-demo}, simule un environnement logiciel où cinq actions sont possibles, notées de 1 à 5. Une boîte de texte affiche à l'écran un historique des actions exécutées, mais cet historique n'est pas gardé sous une autre forme et n'est pas utilisé dans l'algorithme de prédiction. Les cinq actions sont accessibles par le biais de cinq boutons. Pour montrer à l'utilisateur la prédiction évaluée par le système, une couleur est attribuée aux boutons :

\begin{itemize}
  \item Vert pour l'action suivante qui a la probabilité la plus élevée
  \item Un niveau de brun qui indique de pâle à foncé la probabilité, respectivement.
  \item Blanc lorsqu'aucune prédiction ne peut être faite.
\end{itemize}

On peut observer à la figure \ref{fig:markov-chain-demo-case} les prédictions suite à l'action 1 en code de couleur. L'action 3 est la plus probable puisqu'elle a été sélectionnée deux fois suite à l'action 1. L'action 4 est plus probable que l'action 2 (couleur plus pâle), car bien qu'elles ont toutes deux été sélectionnées une fois à la suite de l'action 1, l'action 4 a été sélectionnée plus récemment. Finalement, l'action 5 est brun foncé et est improbable puisque cette action n'a jamais été sélectionnée suite à l'action 1.

L'implémentation de la chaîne de Markov a été faite avec deux dictionnaires emboîtés contenant les mêmes clés: les contrôles d'action (ici des boutons). Chaque dictionnaire lié à un bouton est utilisé comme capture du moment présent et lie des valeurs de probabilité à chacun des boutons. Comme le prototype vise à démontrer la mise à jour en temps réel, il est nécessaire de garder la dernière action exécutée. L'état courant est changé au moment où le modèle est mis à jour, de sorte qu'il n'y a toujours qu'un état gardé en mémoire dans la chaîne de Markov. La valeur de probabilité peut être acquise en utilisant l'action visant la requête comme première clé et l'action potentielle pour laquelle on désire obtenir la valeur en deuxième. L'algorithme \ref{algo:markov-chain-update} démontre la façon de mettre à jour la chaîne de Markov qui est une implémentation de l'équation \ref{eq:bezold-prediction} avec un facteur d'amortissement contant de 0.8.

\begin{algorithm}[h]
 \KwData{\\lc = List[Contrôle]\\action\_precedente \(\in\) lc\\chaine\_markov = Dict[lc, Dict[lc, 0.0]]\\damp\_factor = 0.8}
 \KwResult{Mise à jour de la chaine de Markov}
 \If{action\_precedente \(\notin \emptyset\)}{
 \ForAll{lc as k}{
  chaine\_markov[action\_precedente][k] *= damp\_factor\;
  }
  chaine\_markov[action\_courante][k] += 1.0\;
 }
 action\_precedente := action\_courante
 \caption{Algorithme de mise à jour de la matrice de prédiction}
 \label{algo:markov-chain-update}
\end{algorithm}

L'algorithme utilisé est simple et fonctionne pour un cas typique. Ses limites se situent principalement dans l'absence de détection de séquences (on assume la session d'interaction comme une seule séquence) et une limitation à un seul type de contrôle par chaîne. Cependant, il est possible de constater que les prédictions, notamment pour le cas illustré à la section \ref{sec:user-interface-predictions}, sont efficaces et satisfont les requis d'adaptation. La structure de l'implémentation permet une bonne réutilisabilité puisqu'une chaîne de Markov peut être instanciée pour n'importe quel groupe de contrôle et reste peu coûteuse en terme de mémoire (\(N^2\) du nombre de contrôles) et de performance (\(O(log(N)^2)\))) considérant que la quantité de contrôle reste faible pour un logiciel typique.

\section{Discussion et conclusion}
La prédiction des actions de l'utilisateur est une forme d'adaptivité qui est relativement simple à implémenter et qui peut être utilisée dans divers contextes. Cependant, les prédictions nécessitent la construction d'un modèle représentatif des interactions de l'utilisateur. Il devient difficile de créer de tels modèles de façon efficace et précise lorsque les cibles sont des logiciels plus complexes et des utilisateurs plus expérimentés. Une des raisons est que les utilisateurs expérimentés apprennent à utiliser une interface usager et les changements apportés à l'interface peuvent les agacer et leur nuire dans leur travail. De même, pour les utilisateurs inexpérimentés, les changements induits par les mécanismes d'adaptation peuvent rendre la tâche encore plus difficile puisque le modèle ne couvre pas bien les spécificités de certains utilisateurs. Il devient ainsi très important de trouver des méthodes pour évaluer les modèles utilisés dans le cadre de l'adaptativité et de trouver des façons de générer des modèles qui sont le plus complets possible sans pour autant nécessiter une assurance qualité humaine de tous ses composants.

Bien que les chaînes de Markov sont une façon efficace d'implémenter des prédictions, ces dernières ne peuvent assister l'utilisateur dans l'accomplissement d'un but donné dans le langage du domaine d'utilisation. Par exemple, si un utilisateur peu expérimenté cherchait une façon d'obtenir l'effet de grains de bois cité comme exemple à la section \ref{sec:user-interface-predictions}, le modèle ne contiendrait pas de prédiction relative à la génération de grain de bois. Même si un modèle avait été créé par la compilation de données d'interaction d'autres utilisateurs, il est très possible qu'aucun ou très peu n'aie produit de tel effet. Ainsi, il serait nécessaire de trouver un algorithme permettant de traduire des demandes du domaine d'utilisation (ici l'infographie) en séquences d'actions pour atteindre un but. Ce type de système est souvent appelé ``knowledge-based system''. Ces derniers ont pour but d'inférer logiquement depuis une base de connaissances la véridicité d'énoncés. Ici, il s'agirait ainsi de trouver s'il existe une séquence d'actions produisant un état voulu. La base de connaissances est ici l'espace des actions possibles.

\printbibliography

\end{document}
