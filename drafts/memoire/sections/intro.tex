\chapter{Introduction}
\section{Mise en contexte et problématique}
Depuis plus d'une décennie, les logiciels ont envahi les objets de notre quotidien. L'avènement de l'Internet des Objets (\emph{Internet of Things}), soit l'intégration de microprocesseurs et d'inter-connectivité dans des appareils traditionnels comme des thermostats et des lampes, a grandement poussé cette vague. De plus en plus, les appareils utilisés dans la vie de tous les jours sont contrôlés et dépendent de logiciels. Ce marché émergeant vise à personnaliser ces différents appareils aux préférences et contextes particuliers de chacun des foyers et des utilisateurs.

Cette idée de personnaliser l'expérience des utilisateurs avec leurs appareils n'est pas nouvelle. En effet, il y a déjà plusieurs décennies que le domaine de l'adaptation des logiciels se développe. La vision plus générale est de faire en sorte qu'un système puisse changer son comportement et sa structure interne pour satisfaire à certains critères. Par exemple, un système rencontrant une erreur pourrait prendre des mesures et changer le module fautif pour un autre. Ce principe est appelé << autoguérison >> (\emph{self-healing}) \cite{hinchey2006self}. Plus près de l'utilisateur, un logiciel pourrait personnaliser son interface en fonction du type d'utilisateur ou de son environnement. Un exemple souvent rencontré dans les logiciels existants est l'utilisation de différents types d'interfaces, l'une basique et l'autre avancée. Le choix de l'interface est fait en fonction des caractéristiques de l'utilisateur comme des problèmes moteurs spécifiques ou simplement son degré de familiarité avec le logiciel. Dans ce cas, on parle plutôt d'adaptation basée sur la modélisation des utilisateurs \cite{Montague2012}.

Beaucoup de chercheurs se sont penchés sur les techniques et mécanismes permettant l'atteinte de buts précis par l'adaptation. Cependant, l'intégration de ces mécanismes dans l'ensemble des logiciels reste difficile à faire puisque beaucoup proposent leur propre solution d'intégration, généralement sous forme de \emph{framework}. En plus, plusieurs solutions proposées à ce jour sont directement liées à un domaine d'application précis, typiquement les applications distribuées en réseau. Dans ce domaine, les requis sont souvent rattachés à la qualité de service (QdS) de serveurs et de services. Ainsi, les mécanismes proposés sont difficiles à mettre en place dans d'autres logiciels. Par exemple, les implémentations dans le domaine des interfaces graphiques restent relativement peu explorées et la QdS est interprétée différemment.

Le problème central semble être l'absence de techniques générales pour la mise en place de l'adaptation dans l'ensemble des logiciels et plus particulièrement les logiciels existants. Comme les logiciels deviennent plus répandus et utilisés dans une variété de contextes différents, l'adaptation est devenue requise par nécessité. Il est en effet plus efficace d'adapter une solution générale à des domaines spécifiques plutôt que de développer des solutions complètes spécifiques à chacun des domaines. Cependant, en l'absence de techniques générales, il reste difficile de mettre en place des mécanismes d'adaptation graduellement et d'une façon qui assure la maintenabilité. En plus, comme les logiciels adaptatifs sont différents des logiciels traditionnels par leur organisation déterminée de façon dynamique, les techniques de vérification et de validation (V\&V) existantes sont insuffisantes pour déterminer si une implémentation n'entrera pas dans un état où son comportement est indéfini.

\section{Définition du projet de recherche}
Le présent projet de recherche propose une technique suffisamment générique pour permettre l'application d'une grande variété de mécanismes d'adaptation dans les logiciels et qui reste neutre au niveau des technologies utilisées. Il est important de mettre l'accent sur le but qui est de proposer une façon de mettre en place une \emph{structure de base} dont les modules réalisent les concepts nécessaires à l'adaptation au sens large. Dans le contexte du présent projet, le domaine des interfaces graphiques a été choisi comme cible afin de réduire le cadre de la recherche. Ce domaine a été choisi puisqu'il est de plus en plus soumis à des requis d'adaptation vu l'hétérogénéité des appareils. De plus, la structure typiquement par composants des \emph{toolkits} est largement répandue dans le domaine du logiciel.

Au total, trois patrons de conception sont proposés pour mettre en place une structure de base pour introduire de l'adaptation dans un logiciel.
La schématisation sous forme de patrons de conception est la technique choisie puisqu'elle permet de communiquer des solutions dans un format clair et à un niveau de détail adéquat. Ces schémas montrent l'organisation d'un groupe de modules ayant des rôles précis et qui, utilisés ensemble, offrent une solution à un problème récurrent en logiciel. Dans notre cas, le problème global est l'ajout d'un mécanisme d'adaptation à une application.
Plus précisément, deux des patrons présentés répondent à un problème en lien avec une préoccupation de l'adaptation. Le troisième lie plutôt les deux premiers pour former une structure propice à l'adaptation.

Comme il est également nécessaire d'assurer que la mise en place de l'adaptation est structurellement adéquate, le présent projet inclut la proposition d'une technique de vérification basée sur la structure de base établie. Une technique de V\&V répandue est l'utilisation de métriques de qualité pour évaluer la qualité de façon quantitative. Une métrique offre un indicateur permettant de qualifier le degré de qualité d'une implémentation en fonction d'une préoccupation particulière, par exemple la complexité d'une fonction. Pour ce faire, une métrique permet le calcul d'une valeur et le degré de qualité peut être interprété en comparant cette valeurs à des valeurs bornes (minimales et maximales). Une \mbox{<< bonne >>} valeur dépend de la métrique et il faut généralement minimiser ou maximiser la valeur vers l'une des bornes pour améliorer la qualité.
Le présent projet inclut la proposition d'une nouvelle métrique spécifique aux logiciels adaptatifs. Cette métrique fournit un indicateur de la proportion des états du système qui sont supportés par la structure des composants depuis lesquels ce dernier est formé.

Les gains attendus par l'application de techniques générales pour la mise en place de l'adaptation dans les logiciels sont une meilleure qualité des implémentations et une application des mécanismes d'adaptation dans une plus grande variété de logiciels. Plus particulièrement, l'utilisation de telles techniques devrait minimiser l'effort requis pour l'ajout de comportements adaptatifs dans des projets qui n'en comportent initialement pas. De plus, il devrait être plus facile de comparer différents mécanismes d'adaptation ainsi que différentes implémentations d'un même mécanisme. Dans le contexte des interfaces graphiques, il est attendu que la structure rend la conception et l'implémentation d'interfaces adaptatives plus graduelles et conserve une approche similaire aux techniques existantes dans ce domaine.

\section{Objectifs du projet de recherche}\label{sec:intro-objectifs}
L'objectif principal de la recherche est la création de nouveaux patrons de conception qui représentent des solutions suffisamment générales à des problèmes liés aux préoccupations spécifiques de l'adaptation. De façon plus concise, on cherche à savoir et à montrer :
\emph{Quels patrons de conception permettent l'implémentation graduelle de mécanismes d'adaptation dans les interfaces graphiques?}
Pour atteindre cet objectif principal, il est nécessaire d'atteindre des objectifs secondaires. Ces derniers sont :
\newpage
\paragraph{Proposition de patrons de conceptions}
\begin{enumerate}
  \item Relever de la littérature une banque de patrons de conception liés au domaine de l'adaptation logicielle
  \item Analyser les patrons et formaliser les concepts fondamentaux solutionnant les problèmes communs de l'adaptation logicielle
  \item Déterminer les lacunes et avantages des patrons de conceptions trouvés
  \item Proposer des patrons de conceptions liant les concepts fondamentaux et adressant les lacunes trouvées dans ceux déjà existants
  \item Implémenter les patrons de conceptions afin de valider leur fonctionnement dans une application concrète
  \item Comparer une solution \emph{ad hoc} à une nouvelle utilisant les patrons de conception proposés
\end{enumerate}

De nouveaux patrons de conception permettront de traduire les éléments récurrents qui caractérisent une implémentation de qualité dans le but d'être utilisés dans divers logiciels. Chaque patron aura comme préoccupation un aspect de l'adaptation qui favorisera une meilleure qualité logicielle.

\paragraphss{Proposition de métriques}
\begin{enumerate}
  \item Relever de la littérature une banque de métriques liées à la qualité des logiciels adaptatifs, particulièrement au niveau de la structure
  \item Analyser les éléments liés aux métriques existantes et les pistes proposées par les recherches existantes
  \item Extraire depuis la structure de base proposée des éléments de mesure permettant de quantifier la qualité
  \item Proposer des métriques utilisant des éléments de mesure et qui quantifient un aspect de la qualité structurelle des logiciels adaptatifs
  \item Implémenter le calcul des métriques et valider son utilisation avec des éléments de mesure tirés d'une application concrète
  \item Analyser les résultats des métriques en variant les éléments de mesure dans l'application concrète et proposer des actions correctives
\end{enumerate}

La proposition de la métrique s'inscrit dans l'objectif de proposer une solution \emph{éprouvée}. En effet, il est nécessaire d'effectuer la vérification que le logiciel où les patrons de conception ont été utilisés satisfait des requis de qualité hors de l'atteinte des patrons seuls. Ainsi, la métrique proposée a comme but d'identifier les implémentations qui aboutissent en un système contenant des problèmes.

\section{Contributions originales}
Plusieurs contributions originales sont issues de ce projet. Les trois patrons de conception proposés sont le \emph{Moniteur}, \emph{Proxy routeur} et \emph{Composant adaptatif}.

\begin{itemize}
  \item Le patron \emph{Moniteur}\footnote{Ne pas confondre avec le patron Moniteur utilisé dans le domaine de la concurrence} vise la modélisation, l'acquisition et la distribution de données pour l'adaptation
  \item Le patron \emph{Proxy routeur} vise la réorganisation des chemins d'appels entre différents composants
  \item Le patron \emph{Composant adaptatif} vise la composition d'architectures et de hiérarchies de composants qui ont un comportement adaptatif reposant autant sur la substitution que la paramétrisation
\end{itemize}

Les métriques pour la vérification de logiciels adaptatifs sont la couverture de l'espace d'adaptation modélisée et la couverture de l'espace d'adaptation effective. En plus, des pistes d'analyse et des actions correctives sont proposées pour accompagner les développeurs dans l'utilisation de la métrique.

Pour mettre en pratique les différents patrons et métriques, des développements logiciels ont été réalisés et rendus disponibles sous licence libre du Québec (LiLiQ-P v1.1). Ces derniers sont disponibles aux adresses suivantes :

\begin{itemize}
  \item \textbf{AdaptivePy : } \url{https://gitlab.com/memophysic/adaptive_py_lib}
  \item \textbf{Métriques : } \url{https://gitlab.com/memophysic/adaptive_py_metrics}
\end{itemize}

AdaptivePy est également disponible sur \href{https://pypi.python.org/pypi/adaptivepy}{PyPi}, le répertoire des librairies Python, sous le nom \verb|adaptivepy|\footnote{La librairie peut être installée grâce à l'outil pip avec la commande \verb|pip3 install adaptivepy| sur une distribution GNU/Linux}.

\section{Plan du document}
Ce document est découpé en différents chapitres. Le chapitre \ref{chpt:etat-art} présente l'état de l'art dans le domaine de l'adaptation dans les logiciels et illustre les travaux sur lesquels la présente recherche est basée. Le chapitre \ref{chpt:article1-context} introduit un premier article qui a été présenté dans le cadre de la conférence \emph{Adaptive 2017} et explique en plus grands détails la méthodologie et les développements qui ont permis la proposition des trois patrons de conception. Ce premier article est reproduit au chapitre \ref{chpt:article1}.
Un second article est introduit au chapitre \ref{chpt:article2-context} et les démarches pour l'élaboration des métriques et des solutions pour la vérification de logiciels adaptatifs y sont présentées. Le second article est reproduit au chapitre \ref{chpt:article2}. Une discussion sur les contributions du projet de recherche et leurs implications est le sujet du chapitre \ref{chpt:discussion}. Ce chapitre présente également des idées qui n'ont pas été couvertes dans les articles, mais qui ont tout de même fait partie de la recherche. Le mémoire se conclut avec le chapitre \ref{chpt:conclusion} où un sommaire des travaux est présenté ainsi que diverses propositions pour des travaux futurs.
