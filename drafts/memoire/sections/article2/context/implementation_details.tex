\section{Détails d'implémentation de la couverture de l'espace d'adaptation}\label{sec:metrics-impl-details}
Comme le deuxième article couvre uniquement la définition formalisée et l'utilisation des métriques de couverture de l'espace d'adaptation, la présente section vise à présenter des informations additionnelles concernant leur implémentation. Pour des détails plus précis sur les calculs eux-mêmes, se référer à la section \ref{sec:metrics}.

\subsection{Implémentation de la couverture récurrente}
Dans l'article, l'implémentation de référence de la couverture récurrente (voir tableau \ref{tbl:adaptation-space-measures}, ligne << RSC >>) n'est pas discutée en profondeur. L'implémentation actuelle est considérée comme non optimale, ainsi il est opportun de présenter la solution actuelle et les pistes pour proposer une approche plus performante.

La stratégie actuellement utilisée pour effectuer le calcul est d'itérer dans tous les états définis au moins une fois. Pour ce faire, un espace d'états est généré comme étant l'union des espaces d'adaptation des candidats de substitution. La fonction itère dans les états de ce dernier et, pour chaque état, vérifie le nombre de composants qui le définissent. Si le nombre de composants le définissant est supérieur à 1, le nombre de définitions récurrentes (le nombre total de définitions, moins un) est ajouté à un compteur. Une fois tous les états visités, la couverture récurrente est obtenue en effectuant le ratio entre le compteur et le nombre d'états possibles.

On suppose que cette approche n'est pas optimale puisqu'il est nécessaire d'effectuer la visite par force brute de tous les états définis. Comme une implémentation abstraite des espaces d'états a été faite, il est attendu que des opérations sur ces structures de données puissent fournir une réponse sans nécessiter de visiter chaque état explicitement. Par exemple, une solution pour deux composants serait d'abord d'effectuer l'intersection de leur espace d'états respectif. Ensuite, il suffirait d'effectuer le produit des ratios correspondants au nombre d'états résultant de l'intersection par rapport au nombre d'états possibles de chacun des paramètres définis. Cependant, les calculs pour un nombre \(n\) de composants ne sont pas aussi simples. Comme la performance du calcul de la métrique n'a pas été problématique et que le projet de recherche actuel n'avait pas comme but l'optimisation du calcul des métriques, l'exploration d'une méthode de calcul plus efficace a été réalisée, mais n'a pas donné les résultats escomptés. Il advient donc de travaux futurs à optimiser le calcul de la couverture récurrente qui est le goulot d'étranglement de l'implémentation actuelle.

\subsection{Pseudo-code de l'implémentation}
Un pseudo-code permet de démontrer la façon dont un algorithme est implémenté sans être lié à un langage en particulier. Le pseudo-code de l'implémentation des métriques est donné à l'algorithme \ref{algo:ascoverage}. L'algorithme \ref{algo:monitor-space} présente l'obtention d'un espace d'adaptation avec des moniteurs à la place des paramètres. L'algorithme \ref{algo:primitive-coverage} présente le calcul de la couverture d'un espace d'adaptation.

Il est à noter que lorsque l'algorithme \ref{algo:ascoverage} est utilisé pour effectuer un calcul de couverture effective (le drapeau \verb|effective| est à \emph{True}), le même algorithme de calcul (algorithme \ref{algo:primitive-coverage}) est utilisé. Cependant, les paramètres, qui constituent les clés dans \emph{adaptationSpace}, sont échangés dans l'algorithme \ref{algo:monitor-space} pour les moniteurs correspondants, de sorte que le calcul de couverture reste le même.
Dans l'algorithme \ref{algo:primitive-coverage}, la ligne \ref{lst:line:parameter-possible-values} est générique et fonctionne dans les deux cas, dans la mesure où la classe moniteur est dérivée de la classe paramètre\footnote{Alternativement, pour un langage supportant le \emph{duck typing} comme Python, la méthode \verb|possible_values| définie par les artéfacts Parameter et Monitor du patron Moniteur (voir figure \ref{fig:monitors-uml}) ne nécessite pas de relation d'héritage explicite. Le \emph{duck typing} permet d'utiliser deux classes partageant des définitions de méthodes de la même façon sans nécessiter un lien d'héritage. Dans le cas présent, comme la seule méthode utilisée est \verb|possible_values| et que les deux classes définissent une méthode de ce nom, les deux classes peuvent être utilisées dans le contexte du calcul de la métrique.}.

\vfill

\begin{algorithm}
\caption{Couverture modélisée et effective de l'espace d'adaptation}\label{algo:ascoverage}
\begin{algorithmic}[1]
\Function{CompositeASCoverage}{component, effective, asPrimitive}
\State $coverage \gets$ 0.0
\If {not $asPrimitive$ and $component$ has substitution candidates}
  \If {$effective$}
    \State $candidates \gets$ instances of candidates of $component$
  \Else
    \State $candidates \gets$ classes of candidates of $component$
  \EndIf
  \State $recurrentCoverage \gets$ recurrent coverage of $candidates$
  \State $sumCoverage \gets$ 0.0
  \ForAll{$candidate$ in $candidates$}
    \State $sumCoverage \gets sumCoverage$ + ASCoverage($candidate$, $effective$, True)
  \EndFor
  \State $coverage \gets sumCoverage - recurrentCoverage$
\ElsIf {$component$ has adaptation space}
    \State $space \gets$ adaptation space of $component$
    \If {$effective$}
      \State $space \gets$ monitor space corresponding to parameters in $space$
      \Comment{See algo \ref{algo:monitor-space}}
    \EndIf
    \State $coverage \gets$ adaptation space coverage of $space$
    \Comment{See algo \ref{algo:primitive-coverage}}
\EndIf
\Return $coverage$
\EndFunction
\end{algorithmic}
\end{algorithm}

\begin{algorithm}
\caption{Espace d'adaptation avec moniteurs}\label{algo:monitor-space}
\begin{algorithmic}[1]
\Function{GetMonitorSpace}{adaptationSpace, parameterValueProvider}
\State $monitorSpace \gets$ map of (monitor, state space)
\ForAll{$parameter$, $monitor$ provided by $parameterValueProvider$}
  \State $space \gets adaptationSpace[parameter]$
  \If {$adaptationSpace$ has $parameter$ as key}
    \State $monitorSpace[monitor] \gets adaptationSpace[parameter]$
  \EndIf
\EndFor
\Return $monitorSpace$
\EndFunction
\end{algorithmic}
\end{algorithm}

\vfill

\begin{algorithm}
\caption{Calcul de la couverture d'espace d'adaptation pour un composant primitif}\label{algo:primitive-coverage}
\begin{algorithmic}[1]
\Function{PrimitiveASCoverage}{adaptationSpace}
\State $coverage \gets$ 1.0
\ForAll{$parameter$, $space$}
  \State $possibleValues \gets$ $parameter$'s possible values
  \label{lst:line:parameter-possible-values}
  \State $definedStates \gets$ intersection between $possibleValues$ and $space$
  \State $numerator \gets$ size of $definedStates$
  \State $denominator \gets$ size of $possibleValues$
  \State $coverage \gets coverage * (numerator / denominator)$
\EndFor
\Return $coverage$
\EndFunction
\end{algorithmic}
\end{algorithm}

\newpage
% \subsection{Espace contextuel modélisé et effectif}
% Deux types d'espace contextuel existent :
%
% \begin{itemize}
%   \item \textbf{Modélisé :} Basé sur les valeurs possibles des paramètres
%   \item \textbf{Effectif :} Basé sur les valeurs possibles des moniteurs
% \end{itemize}
%
% Comme les calculs de couverture modélisée et effective sont essentiellement les mêmes mis à part le type d'espace contextuel modélisé, un drapeau \verb|effective_coverage| est utilisé comme argument pour spécifier le type de couverture voulue.
