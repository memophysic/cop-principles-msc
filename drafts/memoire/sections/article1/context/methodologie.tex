\section{Méthodologie}
Le présent projet de recherche a nécessité la recherche et la synthèse de concepts généralisés dans le domaine de l'adaptation des logiciels ainsi que la recherche de patrons de conception utilisant ces concepts. Cette section présente la méthodologie de recherche qui a mené à la rédaction du premier article.

\subsection{Approche générale}
\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{img/iterative-process}
  \caption{Démarche itérative pour le développement des patrons de conception}
  \label{fig:patterns-iterative-process}
\end{figure}

L'approche générale appliquée à ce projet de recherche est basée sur une démarche itérative, autant au niveau de l'analyse que des développements. Cette démarche permet de mettre en pratique les concepts identifiés séparément et d'analyser les limites de leur implémentation. Au fur et à mesure que les concepts sont identifiés dans la littérature, ils sont utilisés dans un contexte réel comme solution partielle et les avantages relevés peuvent alors guider la solution générale qui formera le patron. Un schéma résumant les étapes d'une itération est donné à la figure \ref{fig:patterns-iterative-process}.

Pendant une période donnée d'environ deux semaines, un prototype était développé afin de mettre en place une préoccupation de l'adaptation ou pour en améliorer une déjà mise en place précédemment. Les artéfacts qui sont liés à cette préoccupation peuvent ainsi être ajoutés ou améliorés pour mitiger une lacune identifiée précédemment. Un artéfact fait ici référence à un élément de solution modélisé qui compose un patron. Par exemple, un élément de solution permettant de traduire en langage contextuel de plus haut niveau le contexte actuel depuis des données brutes de capteurs serait considéré comme un artéfact. Pour chaque fin de période, une mise à jour de la synthèse des concepts était produite et utilisée comme base pour proposer une version des patrons de conception. L'essentiel des concepts était considéré comme couvert lorsqu'il était possible de produire une application complète sans nécessairement spécifier un mécanisme d'adaptation précis. Une implémentation de référence des artéfacts tirés des concepts a été faite dans la librairie AdaptivePy, en partie en se basant sur les prototypes des itérations précédentes. Pour mieux évaluer les patrons finaux, une application adaptative graphique a été développée avec et sans la librairie. Les sections suivantes précisent un peu plus chaque point énoncé précédemment.

\subsection{Acquisition et synthèse des concepts essentiels}
Pour proposer des patrons de conception, il est nécessaire d'identifier des concepts fondamentaux qui sont partagés par beaucoup de systèmes adaptatifs. Ces concepts doivent être suffisamment généraux pour être inclus sous forme d'artéfacts logiciels qui seront utilisés pour implémenter différents comportements adaptatifs. Ainsi, des types de comportements adaptatifs doivent être également identifiés et liés aux concepts pour s'assurer que leur implémentation est possible avec les artéfacts en question.

Comme l'approche générale est itérative, l'acquisition et l'analyse de l'état de l'art se sont faites par préoccupation et en même temps que les développements. La première itération a été dédiée à la lecture des enquêtes dans le domaine et des travaux principaux sur la question des patrons de conception, par exemple le mémoire de \citet{ramirez2008design}. Par la suite, une préoccupation cible a été identifiée avant chaque itération afin de concentrer les recherches pendant ces périodes. Les types de travaux analysés inclus les propositions de patrons de conception, de \emph{frameworks}, de modèles, de concepts d'adaptation ainsi que de prototypes présentant des mécanismes d'adaptation.

Pour le premier article uniquement, près de 80 documents ont été analysés et 24 d'entre eux ont été retenus comme apportant des concepts généralisables et originaux. La synthèse des concepts est présentée dans l'article à la section \ref{sec:concepts}. Trois préoccupations sont retenues comme générales : la surveillance des données d'adaptation, les méthodes d'adaptation dans les composants et les stratégies d'adaptation. Il est à noter que les patrons visent principalement les applications graphiques, ainsi il est question plus spécifiquement de \emph{composants} pour les méthodes d'adaptation.

Aussi, ce que la littérature identifie généralement comme \emph{mécanismes} d'adaptation est identifié comme \emph{stratégies} d'adaptation. Cette distinction a été faite pour mettre en évidence la différence entre mécanisme et méthode d'adaptation : une méthode représente la maniabilité de la structure tandis que le mécanisme est la stratégie utilisée par le système  pour atteindre ses buts d'adaptation. En effet, une stratégie tire parti des méthodes d'adaptation disponibles pour atteindre les buts d'adaptation.

Pour déterminer si un concept est généralisable, il devait se souscrire aux requis suivants :

\begin{itemize}
  \item Être neutre par rapport aux mécanismes/stratégies d'adaptation
  \item Être neutre par rapport au domaine d'application
  \item Couvrir une préoccupation de l'adaptation partagée par la majorité des modèles comme MAPE-K et des \emph{frameworks}
\end{itemize}

\subsection{Validation par prototypes}
Afin d'assurer un lien cohérent entre les concepts identifiés et leur application, des prototypes mettant à exécution des préoccupations de l'adaptation ont été produits pour chacune des itérations. Les préoccupations abordées sont :

\begin{itemize}
  \item Spécification des données d'adaptation
  \item Surveillance (\emph{monitoring}) de données d'adaptation depuis \begin{enumerate}
    \item Un fichier XML
    \item Un générateur pseudo-aléatoire
    \item Un fournisseur abstrait
  \end{enumerate}
  \item Spécification du domaine d'adaptation d'un composant
  \item Substitution de composants (via un composant adaptatif)
  \item Filtrage des candidats de substitution
  \item Gestion des instances de candidats de substitution
  \item Intégration de l'adaptation dans une librairie graphique et son éditeur graphique (Qt et Qt Designer, respectivement)
\end{itemize}

Les prototypes ont été développés séparément de la librairie de référence.

\subsection{Développement d'AdaptivePy}
AdaptivePy a été développé en deux temps : d'abord dans une phase exploratoire puis dans une phase de formalisation.
Pour la réalisation des prototypes, les artéfacts nécessaires à leur réalisation ont été développés dans une version préliminaire d'AdaptivePy. Cette version avait comme but d'être une implémentation rudimentaire et possiblement instable des concepts identifiés puisqu'elle ne comptait pas de tests unitaires.

Une fois les limitations identifiées et les problèmes d'implémentation mieux compris, la librairie a été complètement réécrite depuis les patrons de conception formalisés. Cette nouvelle version inclut des tests unitaires ainsi que de la documentation pour la plupart des classes et méthodes. Ainsi, des tests unitaires ont été configurés sur le site de dépôt en ligne de sorte que ces derniers soient exécutés pour chaque \emph{commit}. De plus, comme la librairie est distribuée publiquement, la configuration pour PyPi (base de données de paquets Python) a été ajoutée.

Le développement de cette librairie a participé à l'analyse des concepts lors du procédé itératif du projet de recherche puisque certaines limitations d'implémentations, autant au niveau de la performance que de la structure imposée par la POO, ont influencé l'organisation des patrons de conception. Les lacunes au niveau des artéfacts n'ont donc pas seulement été identifiées au niveau conceptuel, mais également au niveau pratique. Cette approche a permis de produire des patrons de conception qui sont plus facilement applicables et liés aux bonnes pratiques de programmation.
