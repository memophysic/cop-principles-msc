
The adaptation domain of a component is the aggregated domain of all the adaptation data sources it is aware of. For an adaptation data source to be explicitly defined, it must be quantized. We therefore formalize what we call an \emph{adaptation parameter}, which must yield a value within a defined domain of possible measures for a type of adaptation data. An example would be the luminosity as acquired by a some sensor, measured in candela in a discrete range \([a, b]\) with increment \(c\).

The requirement of adaptive components to explicitly define the adaptation domain over which they are defined is summarized in statement \ref{eq:adaptive-requirement}.

\texteq{A component is considered adaptive if it provides a mean to resolve for which adaptation parameters and related domain it is defined.}{eq:adaptive-requirement}

This requirement aims at sharing the knowledge of the adaptivity space of a component. Adaptation mechanisms of a component relying on data parameters it doesn't explicitly define are considered part of the application logic. These mechanisms are then agnostic to any adaptation data from the point-of-view of other components. Because the requirement doesn't force a component to concretely adapt itself, it is assumed by contract: other components have to expect that adaptation may occur on the basis of the defined adaptation parameters, but cannot find out if and how it is accomplished. Alternatively, it is useful to think of this as a valid space for the component to execute; adaptation is a tool to assure validity within this space. The key feature is that a component composed of other adaptive components can know their domain of adaptation. From this fact, we propose that both types of adaptation are in fact complementary. The consequence of this proposal is summarized in statement \ref{eq:complementary-adaptation-types}.

\texteq{Any owner of components with substitution candidates gain an equivalent degree of parametrization.}{eq:complementary-adaptation-types}

In the context of components architecture, substitution candidates are functionally equivalent components in their usage and can be interchanged. Statement \ref{eq:complementary-adaptation-types} describes how a composite component owning substitute candidates providing required services consequently acquire their adaptation space as parametrization. Concretely, the aggregation of all the adaptation domains of functionally equivalent substitution candidates forms a domain of parametric adaptation for their owner.

% Add a figure to demonstrate
\begin{figure}
  \centering
  \includegraphics[width=.5\linewidth]{img/composite_substitute_schema}
  \caption{Example of composite components architecture \newline
  (\mbox{\(C\) = Composite}, \mbox{\(C/S\) = Composite and Substitute}, \mbox{\(S\) = Substitute}, \mbox{\(T\) = Tunable layer}) \newline
  \(T_1\) represents the tunability interface of the two bottom \(S\) components and is known to the \(C/S\) component. Control over the substitution can be provided as an implementation of the tunability interface \(T_2\) provided by \(C/S\) to the top \(C\) component. The \(S\) component in the middle can provide control over its parametrization, but knows nothing of the substitution being applied to \(C/S\). The top \(C\) component is not aware of the \(T_1\) tunability interface, but parametrization it applies through \(T_2\) could trigger parametrization through \(T_1\) subsequently. The final component providing the service can only be one of the three \(S\) components, with the \(C/S\) potentially adding some behavior or adaptation. \(C\) is essentially a proxy to one of the substitutes on lower levels.}
  \label{fig:composite-substitute-schema}
\end{figure}

Substitutes can provide to the component owning them control over their parametrization (e.g. by mean of a tunable interface). This principle can also be applied to each layer of ownership. This means that a component applying substitution can provide to the component owning it control over the substitution process in the form of parametrization. A key guideline for this design to be effective is that the parametrization of a component should be in terms of its abstraction layer. This way, the owning component remains unaware of lower abstractions implementation details. Also, it should be stressed that a degree of parametrization \emph{doesn't imply control} over it; the composite component has the responsibility to provide or not control and decide how it is provided. An example is shown in \mbox{figure \ref{fig:composite-substitute-schema}} with additional explanations of each sub-component's role.

Building on the button vs. switch example, we can see that the implications of this feature is that a composite component at a more abstract level is able to work with a concrete component provided as appropriate for the current context and customize the behavior of its sub-components using explicit parametric adaptation. The mean by which composite components provide parametric control over their composition can easily be realized by using the adapter pattern to adapt the concrete component to a common interface or the decorator pattern to implement the missing adaptation logic (e.g. \(C/S\) component in \mbox{figure \ref{fig:composite-substitute-schema}}).
% Once done, say it will be demonstrated in X section
