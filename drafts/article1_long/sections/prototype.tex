\section{Patterns Realized}\label{sec:realized}
%This section presents AdaptivePy, a reference library implementing the three design patterns presented in this paper, along with a prototype as a case study for analyzing the gains they procure compared to an \emph{ad hoc} implementation.
% As a proof-of-concept for the design patterns, AdaptivePy, a reference Python 3 library, was developed. Then, a GUI-centric case study application was developed with and without the library to demonstrate the gains from using the design patterns compared to an \emph{ad hoc} implementation.
This section aims at providing a more practical foundation for the usage of the patterns presented in Section \ref{sec:design-patterns}.
An overview of AdaptivePy, a library implementing the patterns, is first presented. Then, minimal use cases for the patterns are provided and implemented using AdaptivePy. These should demonstrate how the common problems identified are solved by the patterns and practically put in place.
Finally, an introduction to the use of the patterns for GUI implementations with AdaptivePy and Qt is presented.

\subsection{AdaptivePy}
AdaptivePy implements artifacts from all three design patterns described in this paper. The library is freely available from the PyPi repository (\verb|https://pypi.python.org|) under the name ``adaptivepy'' and is distributed under LiLiQ-P v1.1 license. The Python language was chosen because it is reflective, dynamically typed and many toolkit bindings are freely available. Beyond the patterns, AdaptivePy provides some useful implementations:
% such as enumerated and discrete value parameters, push/pull dynamic monitor decorators, operations over adaptation spaces (extend, union, filter) and an adaptation strategy based on substitution candidates' adaptation space restrictiveness.

\begin{itemize}
  \item Enumerated and discrete-value parameters
  \item Monitor event manager with a global instance as default provider for adaptive components
  \item Polling (pushed values) and pull dynamic monitor as decorators over static monitors
  \item Fixed (always provides the same value) and random static monitors
  \item Methods for operations on adaptation space (extension, union, filter)
  \item Strategy for choosing the most restricted component with narrowest adaptation space for a set of parameters
  \item Automatic computation of aggregated adaptation space for substitution candidates of a proxy router
\end{itemize}

While AdaptivePy is a fully working implementation of the patterns presented in this paper, it is possible to make different choices to realize the artifacts. For example, the MonitorEventManager artifact presented in the \emph{Monitor} pattern could be realized as multiple managers, which coordinate the view on the environment and the propagation of the monitored values. Because the main objective in this paper is to demonstrate the technique for a single-host GUI, a centralized MonitorEventManager was deemed more appropriate.

One area in which special care must be taken when implementing the patterns is to minimize the work necessary to expand the amount of monitors and components. Since a complex system is expected to be composed from hundreds, if not thousands of components, the work necessary to add a new parameter, monitor or strategy must be kept minimal. This challenge is greatly mitigated by the use of a dynamic language like Python, where it is possible to compose classes at runtime.

It is necessary to mention that the aim of AdaptivePy is primarily demonstrative in the sense that it illustrates the applicability of the patterns presented in this paper. While crucial to the success of adaptation in any given application, the end-user's perception of increased usability due to adaptation is not the purpose of AdaptivePy. In fact, the effect of adaptation is expected to greatly vary from one application to the other, depending on what is adapted and when it is triggered. AdaptivePy, and consequently the patterns presented in this paper, aim at counteracting the complexity of implementing different adaptation strategies and structures in a gradual manner. Having each concern changeable and testable as separately as possible is then expected to provide the best end-user perceived usability with maximal predictability and minimal development effort through prototyping and A/B testing.

For each of the three patterns presented in this paper, a small example using AdaptivePy is given and explained.

\subsection{Monitor Pattern}\label{sec:realized-monitor}
As presented in Section \ref{sec:dp-monitor}, the \emph{Monitor} pattern is concerned with acquisition and analysis of adaptation data. To express the environment in terms of contextual data, there is a need to model the environment into data of known range. This quantization is done on some raw data, which could be coming from an hardware sensor, network data provider or any process executing on the host machine (including the monitoring application itself).

An important aspect of the modeling of the environment is that raw data can be further refined into higher-level data through the cascading of monitors. For example, a hardware sensor could provide temperature data ranging from \(-50\degree\) to \(50\degree\)C at \(0.5\degree\) intervals. A higher-level modeling of this data could be to classify the values into an enumeration of three temperature levels: \{ Cold, Normal, Hot \}. Table \ref{tbl:temperature-modeling} shows a possible classification of the temperature levels as provided by the hardware sensor.

\begin{figure}
  \includegraphics[width=\linewidth]{img/temperature_monitors}
  \caption{Realization of a temperature monitoring architecture}
  \label{fig:temperature-monitors}
\end{figure}

\begin{table}[b]
  \centering
  \caption{Suggested Classification of Temperature Levels}
  \label{tbl:temperature-modeling}
  \begin{tabulary}{\linewidth}{|C|C|}\hline
    Level & Range \\ \hline
    Cold & \([-50, 18[\) \\ \hline
    Normal & \([-18, 30[\) \\ \hline
    Hot & \([30, 50[\) \\ \hline
  \end{tabulary}
\end{table}

The artifact from the \emph{Monitor} pattern that allows to provide a range of possible values is the parameter. The state space of the hardware sensor provided data is discrete, expressed as a range with step \([-50, 50[:0.5\degree\)C. The state space of the temperature level is an enumeration with three unique values. These parameters can be monitored by static monitors since they are stateless, assuming the hardware sensor can be queried at any given time. By using this temperature sensor monitor, a second monitor could realize the classification algorithm based on the predefined thresholds presented in Table \ref{tbl:temperature-modeling}. This monitor is considered \emph{complex} because it relies on other monitors.

A refinement to this monitoring structure is to filter the monitored data. In fact, the hardware sensor's raw data might provide subsequent values oscillating between two quantized levels. A smoothing monitor could then be realize to address this issue. A simple moving average filter could be implemented and provide an average of the past \(M\) values. Another approach would be to provide the temperature level, which represents most of the past \(M\) samples. In all cases, because some state is necessary (previous values), the monitor is considered dynamic rather than static. An implication of being a dynamic monitor is that time affects its output value. At any time, a \emph{latest value} can be queried and used in a snapshot of the system's current contextual state.

Fig. \ref{fig:temperature-monitors} provides a summarized view of the monitoring architecture as described previously. AdaptivePy allows to implement this architecture with minimal effort. Listing \ref{lst:parameter-adaptivepy} shows how to declare the identified parameters using AdaptivePy. Similarly, Listing \ref{lst:monitor-adaptivepy} shows how to declare the monitors using AdaptivePy and the threshold values from Table \ref{tbl:temperature-modeling}.

We see from Listing \ref{lst:parameter-adaptivepy} that parameters can be defined to represent adaptation data as state spaces in a trivial way. These parameters are then used in Listing \ref{lst:monitor-adaptivepy} to define the possible values that can be provided by the monitors. We see that the monitors  provide all the possible values of their corresponding parameter. This means that, at runtime, the application can encounter every state of the modeled environment. In this example, we see the cascading feature of the monitors to further refine contextual data into high level adaptation data. The \emph{TempSensorMonitor} acquires raw sensor data, quantized into a discrete range. Then, the \emph{TempQuantizerMonitor} turns this discrete state space into a higher level enumerated state space. Finally, the \emph{TempLvlSmoothingMonitor} applies the \verb|most_common| function to the last five values to smooth variations. A feature of this last monitor is that it is not stateless, which prevents it from being used by other static monitors. It is therefore turned into a DynamicMonitor using a default implementation that is pull-based. This default implementation is realized using the instance decorator \emph{PullDynamicMonitorDecorator}. A pull-based dynamic monitor uses an external scheduling mechanism to perform updates during the lifetime of the application. It is omitted for simplicity in this example.  However, it could trivially be implemented as a button the user has to push or as a polling mechanism using a timer which triggers an update at regular intervals.

\begin{lstfloat}
\begin{lstlisting}[caption={Parameter declaration using AdaptivePy},label={lst:parameter-adaptivepy},language=Python]
RawTemperature = DiscreteParameter(-50.0, 50.0, 0.5)

class TemperatureLevel(EnumeratedParameter):
    low = 0
    medium = 1
    high = 2
\end{lstlisting}

\begin{lstlisting}[caption={Monitor definition using AdaptivePy},label={lst:monitor-adaptivepy},language=Python]
class TempSensorMonitor(Monitor):
    def value(self):
        # Query the hardware sensor
    def possible_values(self):
        return RawTemperature.possible_values()

class TempQuantizerMonitor(Monitor):
    def value(self):
        value = TemperatureSensorMonitor().value()
        if -50 <= value < 18:
            return TemperatureLevel.Cold
        elif 18 <= value < 30:
            return TemperatureLevel.Normal
        else # 30 <= value < 50
            return TemperatureLevel.Hot
    def possible_values(self):
        return TemperatureLevel.possible_values()

class TempLvlSmoothingMonitor(Monitor):
    def __init__(self):
        self._last_five_values = [TemperatureLevel.Cold] * 5
        self._current_index = 0
    def value(self):
        value = TempQuantizerMonitor().value()
        self._last_five_values[self._current_index] = value
        self._current_index += 1
        self._set_latest_value(
            most_common(self._last_five_values))
    def possible_values(self):
        return TemperatureLevel.possible_values()

TempLvlSmoothingDynamicMonitor = \
    PullDynamicMonitorDecorator(
        TempLvlSmoothingMonitor())
\end{lstlisting}
\end{lstfloat}

\subsection{Proxy Router Pattern}\label{sec:realized-proxyrouter}
A proxy router, as presented in Section \ref{sec:dp-proxy-router}, is a component that acts as a proxy and can be controller to route to different delegates. The group corresponding to the possible delegates is called the \emph{substitution candidates} of the proxy router. An important element to the maintainability of an applications is the possibility to change parts related to a concern without affecting others. The \emph{Proxy Router} pattern favorises decoupling of the code related to choosing the appropriate substitution delegate at an appropriate time and how to realize the substitution itself. Because strategies are highly dependent on specific application domains, they are expected to vary from one application to the other. However, the way components substitution can be implemented is mostly independent from the application domain.

A major challenge which transcends application domains is how to determine when adaptation should take place. In GUI, frequent changes in the layout of controls might destabilize users who have learned the position of certain controls. However, a GUI can improve responsiveness and better accommodate various types of users by adapting to their needs. For each scenario, specific strategies for these choices might be implemented and tested. One example of such strategy is to prevent applying adaptation when the user is using the application or a specific feature. By modeling a ``busy'' state for the user, it is possible to create a strategy that is aware of this state and provides the same substitution candidate as before until the user is not busy anymore. Similarly to monitors, one could cascade various routing strategies to create a more complex strategy.

A benefit of cascading strategies is that it allows to create generic strategies that rely on domain agnostic adaptation data such as the busy state discussed previously. Also, it is possible to control the timing of adaptation and computational load through filtering. Using the \emph{Proxy Router} pattern, it is possible to realize multiple strategies and apply them to a common structure which is reusable across applications.

Assuming a parameter ``Busy'' with an enumerated state space of \{yes, no\} and a monitor ``BusyMonitor'' that provides all of the parameter's possible values, one can implement the cascading of strategies with AdaptivePy as shown in Listing \ref{lst:proxyrouter-cascading}. The proxy router \emph{MyProxyRouter} is a trivial proxy router with two substitution candidates: \emph{Candidate1} and \emph{Candidate2}. It uses the \emph{InternalProxyRouter} scheme (see Section \ref{sec:dp-proxy-router}), which implements the router through inheritance. In AdaptivePy, the implementation of the proxy redirects calls to the \verb|__getattr__| method to the delegate object's \verb|__getattr__|, which makes the object behave as if it truly is the delegate. \emph{MyProxyRouter} uses an externally defined strategy for routing that is represented as \emph{MyStrategy} and instantiated in \emph{MyProxyRouter}'s constructor.

An interesting feature of \emph{BusyFilterStrategy} is that it is an adaptive strategy. Being adaptive, it has access to the ``Busy'' state which is being monitored by ``BusyMonitor'', in this case registered to the global monitor event manager. It acquires the ``Busy'' state through a local snapshot, that is a structure regrouping all the states the component is aware of. It does so in the \verb|choose| method and then decides whether the value of the chosen candidate should be updated by querying the cascaded strategy or not.

By breaking down strategies into reusable sub-strategies that can be cascaded, the maintenance and extensibility of an application can be improved. Because it is possible to modify strategies individually and to cascade them as high level blocks in the proxy router, the lack of reusability in strategies is mitigated. Also, the challenge of determining when to adapt can be solved in steps rather than all at once. By gradually adding strategic elements to the proxy router as a common structure, components of an application that were not adaptive can acquire adaptive behavior as substitution candidates and strategies are developed rather than by refactoring the structure each time.

\begin{lstfloat}
\begin{lstlisting}[caption={Proxy router with filter strategy definition using AdaptivePy},label={lst:proxyrouter-cascading},language=Python]
class MyProxyRouter(AdaptiveInternalProxyRouter):
    @classmethod
    def candidates(cls, arguments_provider=None):
        return { Candidate1: lambda: Candidate1(),
                 Candidat2: lambda: Candidat2() }
    def __init__(self):
        super().__init__()
        self._busy_filter = BusyFilterStrategy()
        self._specific_strategy = MyStrategy()
    def choose_route(self):
       return self._busy_filter.choose(lambda:
           self._specific_strategy.choose_route(
               self.candidates()))

@AdaptationSpace({Busy: Busy.possible_values()})
class BusyFilterStrategy(Adaptive):
    def __init__(self):
        super().__init__()
        self._candidate = None
    def choose(self, cascade_strategy):
        busy_state = self.local_snapshot().get(Busy)
        adapt = self._value is None or \
           busy_state == Busy.no
        if adapt:
            self._candidate = cascade_strategy()
        return self._candidate
\end{lstlisting}
\end{lstfloat}

\subsection{Adaptive Component Pattern}
Section \ref{sec:realized-proxyrouter} contained an example of an adaptive component in the form of an adaptive strategy. In fact, the requirements to become adaptive are minimal: define an adaptation space and join and monitoring group by subscribing to a single parameter value provider. From that point on, a component is alerted when state changes within its adaptation space are detected. Also, it can request a local snapshot of the states corresponding to the parameters in its adaptation space. Using these values, it can apply parametric adaptation and, if it is a proxy router, component substitution.

Using the \emph{Adaptive Component} pattern, it is possible to transform a previously non-adaptive component into an adaptive one. AdaptivePy utilizes the Python class decorators semantic to inject an adaptation space to any class. Then, by inheriting from the \emph{Adaptive} class, a component can join a specific a parameter value provider by specifying it in the \emph{Adaptive} constructor. This parameter value provider is realized using the MonitorEventManager from the \emph{Monitor} pattern. It can then subscribe to any of the parameters in its adaptation space.

\begin{lstfloat}
\begin{lstlisting}[caption={Adaptive component definition using AdaptivePy},label={lst:adaptive-component-realized},language=Python]
from adaptivepy.state_space.enumerated_state_space import EnumeratedStateSpace as Ess

@AdaptationSpace({TemperatureLevel:
    Ess({TemperatureLevel.Low,
         TemperatureLevel.Normal})})
class TempAdaptiveComponent(Adaptive):
    def __init__(self, parameter_value_provider=None):
        super().__init__(parameter_value_provider)
        self._subscribe_to_all_parameters()
    def updated_monitored_value(self, parameter, old_value, new_value):
        # Implement parametric adaptation
        if new_value is TemperatureLevel.Low:
            ... # Do something
        else: # new_value is Temperature.Normal
            ... # Do something else
\end{lstlisting}
\end{lstfloat}

The declaration of a simple adaptive component is presented in Listing \ref{lst:adaptive-component-realized}. Reusing the monitors from Section \ref{sec:realized-monitor}, the adaptive component \emph{TempAdaptiveComponent} uses the temperature to adapt parametrically in the \verb|updated_monitored_value| method. We see that, contrarily to the other examples, the adaptation space does not fully cover the parameter's possible values. Because it is not supported, the implications of reaching the \verb|TemperatureLevel.Hot| state are undefined for this component. If an application uses this component and can reach this state, component substitution should be implemented to swap this component with another one which supports the missing states. An advantage of this design is that a developer can focus on an explicitly defined region of an application's adaptation space and ignore other states in their implementation. This is seen in the implementation of the \verb|updated_monitored_value|, where only the states defined in the adaptation space are handled. In this way, if the \verb|new_value| is not \verb|TemperatureLevel.Low|, it can only be \verb|TemperatureLevel.Normal|. This is the case because the component has no knowledge of \verb|TemperatureLevel.Hot|.

By specializing adaptive components, the service they offer is expected to be better suited at the region of adaptation space they define. By adding specialized component and adaptive behavior to non-adaptive components, an application can be ported to an adaptive form gradually. Also, because the patterns presented in this paper serve specific concerns, the adaptive components are not expected to be affected by changes in the monitoring or their structural arrangement when used for an application. The latter is possible because of the basic structure provided by the proxy routers and by the self-contained nature of components-based software.

\subsection{Patterns applied to GUI}\label{sec:workflow-gui}
The patterns presented in this paper can be applied to GUI to create adaptive components as custom widgets and layouts. The general-use toolkit Qt was chosen for the case study, therefore this section will focus on Qt implementations. Qt provides a graphical editor, Qt Designer, for designing the GUI in a language independent descriptive language. Since this is the \emph{de facto} approach, it is also the favored workflow. Note that this is true for many other toolkits (e.g., Gtk with Glade, JavaFX with SceneBuilder).

An \emph{ad hoc} solution would be to add a placeholder widget in the GUI and replace them at runtime with the adequate component. Setting the appropriate control needs to be done entirely programmatically, along with any customization necessary, in the window's class that owns the control. This leads to a lack of extensibility, a tangling of concerns between the adaptation concern and the components' own concern. Moreover, the approach is not compatible with normal GUI design workflow, which involves previewing the application in the graphical editor before adding logic.

By controlling monitors from the \emph{Monitor} pattern, one can visualize any adaptation done by components for the given toolkit. If a different toolkit is to be used (e.g., when porting an application), the necessary work is to create a candidate component for a proxy router using the new toolkit and adding a toolkit parameter value as an adaptive space definition. A conversion from one description language to the other would also be needed. As for the structure provided by the \emph{Proxy Router}, only the binding to the toolkit's widget replacing logic is to be ported. As for the \emph{Adaptive component} pattern, the components simply need to support the adequate portion of the adaptation space, which includes a toolkit parameter if any adaptation logic is dependent on different toolkit.

In this paper, because Python is used rather than C++ (Qt's native language), an external plugin for Qt Designer is necessary to load custom widgets. This is provided by PyQt as ``libpyqt5'' for GNU/Linux. Custom widgets are created using the \verb|QPyDesignerCustomWidgetPlugin| base class. Fields can also be added using \verb|pyqtProperty| and use the underlying adaptive component's interface to customize the component. This is especially useful with proxy router components since any customization is automatically applied to any candidate and state transfer can be more easily handled. It is also possible to use properties to control adaptive behavior by means of exposed knobs.

\section{Prototype}\label{sec:prototype}
Adaptivity can help in improving usability in different ways. One usability principle of graphical user interfaces is to take into account the user's cognitive limitations into consideration for the presentation of controls. For example, the number of elements in a group one can remember from short-term memory is used to limit the number of grouped controls displayed to the user. This number is not confidently known, but some suggested that chunks of \(4 \pm 1\) elements can be accurately remembered using short-term memory, while it was originally estimated to be averaging around \(7 \pm 2\) \cite{cowan2001}. We draw inspiration from this usability principle in our case-study prototype application.

The case study application is a special poll designed to favor polarization. Five yes/no questions are asked to a user and answered by selecting the most appropriate response among a list of options. The possible options provided include yes, no, mostly yes, mostly no and 50/50. To favor polarization, statistics from the previous answers are used to restrict the range of options provided to the user. If the polarization is judged insufficient because of mixed responses (low polarization), fewer options are provided. On the contrary, if virtually all users have answered yes (high polarization), more options in between will be given. The workflow of the application is to start the ``quiz'' using a Start button, choose appropriate options and send the form using a Submit button. If some options remain unselected, a prompt alerting the user is shown and the form can be submitted again once all options are selected.

The adaptation used is a form of \emph{alternative elements} \cite{bezold2011adaptive}. This provides a form of ``plastic'' GUI in that it adapts itself, but retains its usability \cite{Coutaz2010}. The GUI is made plastic by replacing control widgets displaying the available options at runtime, conserving the option selection feature in any resulting interface. To minimize the visual overload, some widgets are more appropriate than others to display them, while some cannot display certain amounts of options. A checkbox can handle two options with a single control. Radio buttons could handle many options, but to follow the usability guideline of congnitive limitation, we could use it to display up to four options at a time. Finally, a combo box can handle many options, but it does not display all the options on the window unless it is clicked. For our usage, it is a better choice for five and more options. Of course, radio buttons can hold more options and the combo box less, but the amounts suggested represent the ranges they better suit the usability principle. Because many other variables need to be taken into account and affect the usability, the ranges can be chosen by a designer and further refined through user testing, which means they must be easy to edit.

Polarization levels act as adaptation data to drive adaptation. An appropriate solution would allow to design the GUI within Qt Designer and to preview of the adaptation directly, rather than having to add the business logic beforehand. It would also allow for gradual addition and modification of control widget types without necessitating changes in unaffected modules.

The toolkit used for this application is Qt 5 through the PyQt5 wrapper library. It is a cross-platform toolkit library, which provides implementations of widgets like checkboxes, combo boxes are radio buttons groups. The concrete work is therefore limited to implementing how these components can replace each other at the appropriate time and how they are included in a main user interface. We are therefore more interested in the underlying structure of adaptation within the application than specific adaptation strategies and their user-perceived effectiveness. Once an appropriate structure is in place, we expect these can be more easily devised, tested and improved.
