\section{Comparing Ad hoc and AdaptivePy}\label{sec:results}
\begin{figure}
  \includegraphics[width=\linewidth]{img/poll_app}
  \caption{Adaptive case study application ``Polarized Poll''}
  \label{fig:poll-app}
\end{figure}

The windows shown on Fig. \ref{fig:poll-app} are the resulted GUI for the application in all three polarization states. Because this case study's focus is on GUI, the monitoring of past responses was simulated and a random monitor is used instead. This monitor updates its value by means of a polling dynamic monitor every second, allowing to easily observe adaptation.

% Typically, a floating-point monitor with range \mbox{[-1, 1]} would compute an average of past answers with a numerical value\footnote{No: -1.0, Yes: 1.0, Mostly no: -0.5, Mostly yes: 0.5, 50/50: 0.0} and a discrete monitor, here the polarization monitor, would analyze the values and quantize them using three ranges\footnote{Low: \(0.0 \leq |x| \leq 0.33\), Medium: \(0.33 < |x| \leq 0.66\), \mbox{High: \(0.66 < |x| \leq 1.0\)}}.

To emphasize the differences between the \emph{ad hoc} solution and the one using patterns, adapters for each three control widgets (checkbox, radiobox and combobox) were created and are used in both applications. They all implement a common interface \verb|OptionsSelector|, which defines common operations on the controls such as \verb|set_text| for the question labelling and \verb|set_options| that takes pairs of text and corresponding value for averaging past answers. The goal is to compare the implementation of adaptation rather than adding new type of adaptation, thus the same control abstraction approach was used for controls in both cases.

\subsection{\emph{Ad hoc} Application}
A simplified UML diagram of the \emph{ad hoc} implementation is shown on Fig.  \ref{fig:classic-gui-quiz-uml}. The chosen approach is to add placeholder widgets in QuizMainWindow which will be substituted by an appropriate component instance at runtime: CheckboxQt, ComboboxQt or RadioboxQt. A polarization level defined in the enum Polarization is bound to each of these types. A timer within QuizMainWindow polls the polarization value and calls \verb|set_options_selector_components| with the appropriate type. Adaptation control, along with any customization necessary, is entirely done in QuizMainWindow.

\begin{figure}
  \includegraphics[width=\linewidth]{img/classic_gui_quiz}
  \caption{Simplified UML diagram of \emph{ad hoc} implementation of case study application}
  \label{fig:classic-gui-quiz-uml}
\end{figure}

Fig. \ref{fig:designer-adhoc} shows Qt Designer as the main window is created for the \emph{ad hoc} implementation. Notice that because placeholder components are blank, no feedback is given to the designer. It is therefore not possible to test the controls or set the question label. This makes the approach incompatible with the usual GUI design workflow, which involves previewing the application in the graphical editor before adding business logic.

\begin{figure}
  \includegraphics[width=\linewidth]{img/designer_adhoc}
  \caption{Qt Designer using plain widgets as placeholder for \emph{ad hoc} implementation}
  \label{fig:designer-adhoc}
\end{figure}

When analyzing the \emph{ad hoc} code, it is obvious that separation of concerns is not respected since the option selection logic is tangled to its owner element, the main window. Concerns such as scheduling for recomputing polarization and component substitution are mixed with GUI setup and handling of the business flow. This leads to a lack of extensibility, a tangling of concerns and limits unit testing of components. A method is used to select which control component to use based on the polarization, but this solution remains inflexible. The knowledge of adaptation is hidden and cannot be used to devise portable strategies.

One of our goals is to gradually add adaptation mechanisms to GUI implementations, but this is difficult since modification of important classes will add risk of introducing defects. Also, there is no easy way to work on adaptation mechanisms separately from the application. In fact, we cannot separately test the adaptation logic and integrate it after.
Another limitation, in this case specific to GUI, is that all settings specific to the widgets (e.g., question labels) cannot be set from the graphical editor. This is a strong deviation from the usual GUI workflow.
Generally, the lack of cohesion induced by the inadequate separation of concerns is a sign of low code quality. Because no adaptation mechanism can easily be introduced, modified and reused in other projects, the \emph{ad hoc} implementation works for its specific application case, but is subject to major efforts in refactoring when requirements and features will be added throughout its development cycle.

\subsection{Application Using AdaptivePy}\label{sec:results-app-adaptivepy}
A simplified UML diagram of the application is shown on Fig. \ref{fig:adaptive-gui-quiz-uml}. From it, we see that the polarization is a discrete parameter and is used by AdaptiveOptionsSelector, specifically to define its adaptation space based on the ones provided by its substitution candidates: CheckboxQt, ComboboxQt and RadioboxQt. Additionally to adaptation by substitution, RadioboxQt can parametrically adapt to changes of polarization levels \{low, medium\}, since they respectively correspond to 2 and 4 options. Its behavior is that the appropriate number of options is shown depending on the polarization level. AdaptiveQuizMainWindow is free of adaptation implementation details and simply uses the AdaptiveOptionsSelector instances as a normal OptionsSelector. OptionsSelectorQt is a subclass to AdaptiveOptionsSelector, which is used as a graphical proxy to candidate widgets. It also defines properties used in Qt's graphical editor Qt Designer, in this case the question label.
%The monitor used to provide polarization values is a polling dynamic monitor decorator over a static random monitor and registered to a global MonitorEventManager instance. Like in the \emph{ad hoc} implementation, the monitor is simulates data in order to observe frequent adaptation.

Every AdaptiveOptionsSelector instance is made a subscriber to the QuizOptionPolarization parameter at initialization. They are updated when a change in the monitored value is detected, i.e., when a monitor detects a value is different from the previous one. This is because identical subsequent parameter values are expected by default to lead to the same state, so they are filtered out. In the case of AdaptiveOptionsSelector, because it is a proxy router, \verb|choose_route| is called to determine which substitution candidate to route to. Prior to using an adaptation strategy to select the most appropriate candidate, inappropriate ones can be filtered out using \verb|filter_by_adaptation_space|. This function, provided by AdaptivePy, takes a list of candidates along with a snapshot of the current monitoring state and only returns those with adaptation space supporting the current context. Then, a strategy like \verb|choose_most_restricted| is used to choose among valid components. If no component is valid, an exception is raised. With a candidate chosen, all that remains is configuring the proxy router by calling the \verb|route| method with the chosen candidate. This method must also take care of state transfer between the previous and new proxied components. This feature is already defined in the common interface OptionsSelector as \verb|state_transfer|.
%During the routing process, components for which the current situation of polarization level does not correspond to their adaptation space are filtered out. In this prototype, the \verb|choose_most_restricted| strategy is used to choose among valid components.
The \verb|route| method takes care of the state transfer and updates the proxy (done by the library). Subscription to the polarization parameter is done at initialization.
%All widgets are alerted individually to adapt themselves upon the reception of a parameter value different from the previous one. This is because identical subsequent parameter values are expected, by default, to lead to the same state, so they are filtered out.

\begin{figure}
  \includegraphics[width=\linewidth]{img/adaptive_gui_quiz}
  \caption{Simplified UML diagram of case study application implementation using AdaptivePy}
  \label{fig:adaptive-gui-quiz-uml}
\end{figure}

Fig. \ref{fig:designer-adaptive} shows Qt Designer as the main window is created with the AdaptivePy-based implementation. When compared to Fig. \ref{fig:designer-adhoc}, we notice that the designer has a full view of how the application will look. Moreover, the currently displayed adaptation can be controlled through the setup of the monitors. For example, it is possible to replace the random value by one acquired from a configuration file and trigger adaptation manually. Also, each question is simply a OptionsSelectorQt component rather than a placeholder component and the question is entered directly from the graphical editor using the label property (bottom-right). A major advantage is that adaptive components can be reused in other interfaces because they are provided as standalone components. The need for easy edition of adaptation spaces is also addressed by modifying or overriding the \verb|adaptation_space| method of adaptive components.

\begin{figure}
  \includegraphics[width=\linewidth]{img/designer_adaptive}
  \caption{Qt Designer using adaptive components developed with AdaptivePy}
  \label{fig:designer-adaptive}
\end{figure}

The main difference compared to the \emph{ad hoc} implementation is that no adaptation concern can be found in the owner class, the main window. An enumerated discrete parameter with three polarization values (low, medium and high) is monitored by a polling dynamic monitor decorator over a random static monitor.
 % based on a QTimer which is directly used by the \emph{ad hoc} version, was implemented rather than using AdaptivePy's provided one. This was necessary because AdaptivePy uses Python threads, but Qt objects need to be manipulated through Qt's own threads.
To create the adaptive component, an adaptive proxy router was used as a base with candidates being the three same control classes as the \emph{ad hoc} implementation, but in adaptive form (each having an adaptation space related to different polarization levels).

To create the custom widget, two classes are necessary: a dedicated class, which inherits QWidget and its related factory with metadata used by Qt Designer such as its name, group and description. A template class was created to ease this step. The only logic specific to the options selector is related to additional properties. In this case, a string property for the question label along with binding to the options selector's interface method (getter and setter) is used. The option values are also set by this class to centralize the customization logic (this could also have been done using a property). Finally, the logic regarding the lay outing was implemented as a layout with a single element: the proxy delegate. When routing, the QWidget adapter removes the proxy delegate from the layout and adds the new one provided by the base option selector class (described previously). No additional modification other than removing the logic related to the old options selector implementation was necessary.

The adaptation logic is essentially located in the adaptive proxy router class: AdaptiveOptionsSelector. Because adaptation is separated from the rest of the business logic, the main window class can use the adaptive components without the knowledge of adaptation. The only logic remaining is with regard to buttons handling (Start and Submit buttons). It is clear in this implementation that the knowledge of adaptation space, which was hidden in the \emph{ad hoc} implementation, is used to efficiently choose a substitution candidate.
More so, the radiobox is suitable for two to four options and therefore covers low and medium polarization through parametric adaptation. It could then be used instead of the checkbox if a strategy for choosing the less restricted candidate had been used or if a malfunction is detected in the checkbox implementation rendering it inadequate as a candidate. This parametric adaptation behavior cannot easily be included in the \emph{ad hoc} implementation since the knowledge of polarization is kept at the owner component level. The component would need to provide a mean through its interface to customize a component based on polarization, but this would affect all other components as well.

Self-healing action such as replacing a failing component can be realized by monitoring the components and including this logic as a strategy. This is not easily realizable in the \emph{ad hoc} implementation.
In the prototype, a radio box could safely replace a checkbox since it parametrically covers its full adaptation space, overlapping on \{low\} polarization. Also, from this case study, we can see that arbitrarily large hierarchies of adaptive and non-adaptive components can be built without tangling code or affecting other components when adding new adaptive behavior.
