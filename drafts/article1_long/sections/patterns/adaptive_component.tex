\subsection{Adaptive Component Pattern}\label{sec:dp-adaptive-component}\noindent
\textbf{Classification:}
Analyze and plan.\\
\textbf{Intent:}
Use monitored adaptation data to control parametric adaptation and component substitution by making adaptation spaces explicit.\\
\textbf{Motivation:}
A basic structure is needed to easily add adaptive behavior in the form of parametrization or substitution.
Components need a way to explicitly provide means for adaptation strategies to reason about their adaptation space in order to formulate plans. This information should be external to a base component if the adaptation is to be added gradually. Most importantly, an adaptive component must behave like any non-adaptive component and be used among them without side effects on the rest of the system.
Complementarily to monitors, which provide values within a domain explicitly defined by a parameter, components require a certain domain of values they support and are expected, by contract, to adapt themselves to (parametrically or by substitution).
This domain is an \emph{adaptation space} that can be reasoned about to devise efficient adaptation strategies.
\\
\textbf{Participants:}
\begin{itemize}
  \item \textbf{Adaptive:} An adaptive component that defines means for acquiring the adaptation space. It can be used as a subscriber to a parameter value provider.
  The adaptation space is a dictionary of parameters with a set of values it supports. To acquire monitored values, it has a reference to one and only parameter value provider. It can therefore subscribe to a parameter and receive updates when a new value is detected, triggering parametric adaptation when needed. If an unexpected value (outside its adaptation space) is received, an exception can be raised and some higher-level adaptation mechanism can be fired (e.g., substitute the component for another one).
  \item \textbf{Monitor event manager:} Parameter value provider realized with the monitor pattern (see Section \ref{sec:dp-monitor}).
  \item \textbf{Parameter value subscriber:} Provides a means to be notified when a new value of a parameter it has subscribed to has been acquired (see Section \ref{sec:dp-monitor}).
  \item \textbf{Proxy router:} Proxy router pattern (see Section \ref{sec:dp-proxy-router})
  \item \textbf{Adaptive proxy router:} Adaptive version of a proxy router allowing to drive the routing process (substitution) using monitored data.
\end{itemize}
\textbf{Structure:}
Fig. \ref{fig:adaptive-component-uml} shows the structure of the adaptive component pattern as a UML diagram.
\begin{figure}[h]
  \includegraphics[width=\linewidth]{img/adaptive_component}
  \caption{Adaptive component pattern UML diagram}
  \label{fig:adaptive-component-uml}
\end{figure}~\\
\textbf{Behavior:}
A component to be made adaptive can inherit the adaptive interface or a specific decorator can be created if the component's code should remain unchanged. The adaptive implementation defines what base adaptation space it will support.
For example, in GUI implementations, this could be the availability of a toolkit or the type of input medium used (e.g., touch screen, mouse/keyboard, pen).
Then, knobs can be defined within the component and used as  variables to compute, for example, its size or lay outing specifications. Tuning can be done when an updated parameter value is received.
For substitution, the process is the same, but uses the AdaptiveProxyRouter interface.
% Because it inherits the Adaptive interface, modifying a component exhibiting only parametric adaptation to add substitution is straightforward.
Specific strategies can be created, using as many generic filters as possible (e.g., filter out candidates with adaptation space not overlapping with a snapshot of the current state).
% New knobs could be added to customize a strategy instance or choose which strategy to apply.
\\
\textbf{Consequences:}
Because of the explicit declaration of adaptation space, strategies can be reasoned about how a component can behave in a situation. For example, a strategy can use the fact that a component's space is too specific or too wide.
A significant advantage is that this can be previewed and tested by mocking the corresponding monitors (assuming that the designer's device has the adequate dependencies).
Any component can be made adaptive and does not require modifications to other components.
Even a parameter value provider can become gradually more complex. It could initially be based on a configuration file, which is essentially static during the application's execution, and be replaced by a more elaborate one when needed.
Because of the support for both parametric adaptation and component substitution, the basic structure proposed in this pattern is suitable for virtually any adaptive mechanism based on monitored data and components adaptation spaces.\\
\textbf{Constraints:}
Like stated in Section \ref{sec:dp-monitor}, interacting adaptive components must subscribe to the same parameter value provider to assure consistency in decision-making processes. While arbitrarily large hierarchies of adaptive components can be composed, there is an inherent overhead induced in the adaptation and routing process. Because a component subscribing to some parameter value provider such as the monitor event manager has no guarantee that this parameter is being actively monitored, adaptive components need to define a default behavior or immediately request a snapshot of the current state.
If exceptions are used for non-monitored parameters (no value in the snapshot), their handling should be carefully done based on how monitors are registered (e.g., if monitors are concurrently registered as components are created).
To minimize this effect, it is preferable to register monitors prior to creating any adaptive component.\\
\textbf{Related patterns:}
Monitor (\ref{sec:dp-monitor}), proxy router (\ref{sec:dp-proxy-router}),
adaptive component \cite{Chen2001},
virtual component \cite{Corsaro02virtualcomponent}.

%
% The adaptive component pattern is shown in figure \ref{fig:adaptive-component-uml}. The basic component contract is enforced by the \emph{Adaptive} interface which provides the adaptation space (\verb|defined_context()|) and explicitly defines a single monitoring group (\verb|monitors_provider()|) which should remain immutable unless indicated otherwise by the implementing classes. These two methods satisfy, respectively, requirement of statement \ref{eq:adaptive-requirement} for a component to be considered adaptive and requirement of statement \ref{eq:share-monitor-group} to assure consistency for local context knowledge.
%
% The two types of adaptation presented in Section \ref{sec:types-adaptation}, parametrization and substitution, can be realized directly by using this design pattern:
%
% \paragraph{Parametrization}
% Parametrization is done by implementing the Adaptive interface and using the monitor provider to acquire monitors for the parameters of the component's defined context to adapt its internal behavior.
%
% \paragraph{Substitution}
% Substitution is done by using the abstract classes AdaptiveSubstitute and AdaptiveProxyRouter. An AdaptiveSubstitute is the implementation of a substitute as described in \ref{sec:types-adaptation}: a specialized service provider which can substitute another one obeying the same service interface. An AdaptiveProxyRouter gets its name from the fact that it acts as a \emph{proxy} for a substitute and that it has for concern the logic of \emph{routing} its service calls to the most appropriate substitute. It is further referred to in a generic form as a \emph{proxy router}. It is essentially an extension of the Strategy pattern with explicit delegation and adaptation data used as a basis for routing to the appropriate strategy.
%
% In this pattern, a substitute implicitly joins a monitoring group by obtaining a MonitorsProvider instance from its parent proxy router by dependency injection. The monitors provider can be a different instance than the proxy router's, but must obey the one-to-one relationship of parameter and monitor. This allows, for example, the proxy router to manually synchronize value acquisition between its child components using a monitor proxy (this is further explained in Section \textbf{???}). In general, the reference to the monitors provider can be retrieved through the method \verb|monitors_provider()|. For proxy routers, a static version of this method called \verb|composite_monitors_provider()| is defined to allow for static resolution of adaptation space. % Discuss more on this later
% The monitors provider returned by \verb|monitors_provider()| is in fact a monitoring group manager for localized components. It is used by components within a common context to acquire necessary monitors corresponding to their defined context's parameters. It should be noted that a MonitorsProvider instance might be unable to provide a monitor for a given parameter, in which case a None value is provided. The absence of monitor implies that no decision based on the related parameter can be made, a situation which might or might not be critical depending on the application service. However, the component can take action to adapt or deactivate dependent features.\\
%
% The usage of the pattern in a hierarchic architecture is its main advantage. Composite components with control over their sub-components parametrization\footnote{This feature can be achieved by the addition, either statically or dynamically, of knobs to the common service interface.} can offer control to their owner over their own parametrization. In the case of a proxy router, parametrization could be used to customize how substitution is applied. Adaptive behavior is therefore easily separable in specialized components which, once assembled, form a complex adaptive component over a large spectrum of parameters. Also, because parametrization can be applied to a specific components, there is no need to have all components exhibiting or specifying adaptation in order to implement adaptive behavior: only the components responsible for the behavior to be adapted. As stated in the substitution part of Section \ref{sec:types-adaptation}, components which don't specify an adaptation space are simply considered agnostic to all adaptation parameters. The gradual integration of the pattern is therefore fully compatible with the  incremental nature of software development.
%
% We chose to make limits and capabilities of adaptation mechanisms explicit, similarly to how we made explicit adaptation data monitoring and its domain of application. This allows our components to have insight on their actual degree of adaptability depending on the monitors' domain compared to its own. Also, because adaptive components are required to provide a mean to acquire knowledge of their adaptivity space, a composite component's adaptivity space can automatically be derived as a superset of all its sub-components' adaptation space. A useful consequence of this design is that, much like monitors, it can be applied hierarchically to build arbitrarily complex adaptive components without having to worry about reconciling composite adaptation space each time manually.
