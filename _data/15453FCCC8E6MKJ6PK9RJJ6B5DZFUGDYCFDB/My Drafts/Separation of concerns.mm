<map version="docear 1.1" dcr_id="1461700839929_d6mw1djowbeb8urgyd9gmwjmz" project="15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB" project_last_home="file:/Users/memo/Docear/projects/cop-principles-msc/">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="Separation of concerns" FOLDED="false" ID="ID_414415608" CREATED="1461700839893" MODIFIED="1461700839899">
<hook NAME="AutomaticEdgeColor" COUNTER="1"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Architecture of user interfaces" POSITION="right" ID="ID_1984715533" CREATED="1461700856243" MODIFIED="1461700864693">
<edge COLOR="#ff0000"/>
<node TEXT="Patterns" ID="ID_1706523516" CREATED="1461700870776" MODIFIED="1461700874891">
<node TEXT="MVC" ID="ID_1041749499" CREATED="1461700883589" MODIFIED="1461700889738">
<node TEXT="Separation of Concerns for User Interfaces" ID="ID_1527233330" CREATED="1461700425926" MODIFIED="1461700425927" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/user-interface/vub-prog-phd-08-02.pdf">
<pdf_annotation type="BOOKMARK" page="19" object_id="2767138467119141293" object_number="11" document_hash="45AF1A3BD546466FFEEA666AD4A56B6CFAC85E98ECFA471CB8C319E62AF7AD2">
    <pdf_title>On the Separation of User Interface Concerns</pdf_title>
</pdf_annotation>
<attribute NAME="authors" VALUE="Goderis, Sofie"/>
<attribute NAME="title" VALUE="On the Separation of User Interface Concerns"/>
<attribute NAME="year" VALUE="2008"/>
<attribute NAME="key" VALUE="goderis2008separation"/>
</node>
</node>
</node>
</node>
</node>
</map>
