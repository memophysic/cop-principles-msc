<map version="docear 1.1" dcr_id="1463251114424_bnwl1rw2sxc721bo4j9xj5pw7" project="15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB" project_last_home="file:/Users/memo/Docear/projects/cop-principles-msc/">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="Adaptive behavior design pattern" FOLDED="false" ID="ID_1027537468" CREATED="1463251114348" MODIFIED="1463334120058">
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true" show_icon_for_attributes="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Intro" POSITION="right" ID="ID_679148300" CREATED="1463251182144" MODIFIED="1463251184822">
<edge COLOR="#ff0000"/>
<node TEXT="CBSE design used to implement adaptivity in softwares" ID="ID_1864931064" CREATED="1463251186887" MODIFIED="1463251218683">
<node TEXT="3.1.2 Component based software engineering (CBSE)" FOLDED="true" ID="ID_1003726279" CREATED="1462856089155" MODIFIED="1462856089155" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/Self-adaptive%20systems-%20A%20survey%20of%20current%20approaches,%20research%20challenges%20and%20applications.pdf">
<pdf_annotation type="BOOKMARK" page="3" object_id="4753707466315189930" object_number="1249" document_hash="2510D7C6FC9B18AD27E31D162CA1B4117B87AF42285487BEAE1BA2BBEDE1294E">
    <pdf_title>Expert Systems with Applications</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="MaciasEscriva20137267"/>
<attribute NAME="journal" VALUE="Expert Systems with Applications "/>
<attribute NAME="authors" VALUE="Frank D. Mac&#xed;as-Escriv&#xe1; and Rodolfo Haber and Raul del Toro and Vicente Hernandez"/>
<attribute NAME="title" VALUE="Self-adaptive systems: A survey of current approaches, research challenges and applications "/>
<attribute NAME="year" VALUE="2013"/>
<node TEXT="CBSE example implemnetation" ID="ID_329213049" CREATED="1462856089762" MODIFIED="1462856089763" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/Self-adaptive%20systems-%20A%20survey%20of%20current%20approaches,%20research%20challenges%20and%20applications.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="3" object_id="5232861359418682534" object_number="124" document_hash="2510D7C6FC9B18AD27E31D162CA1B4117B87AF42285487BEAE1BA2BBEDE1294E">
    <pdf_title>Expert Systems with Applications</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="MaciasEscriva20137267"/>
<attribute NAME="journal" VALUE="Expert Systems with Applications "/>
<attribute NAME="authors" VALUE="Frank D. Mac&#xed;as-Escriv&#xe1; and Rodolfo Haber and Raul del Toro and Vicente Hernandez"/>
<attribute NAME="title" VALUE="Self-adaptive systems: A survey of current approaches, research challenges and applications "/>
<attribute NAME="year" VALUE="2013"/>
</node>
<node TEXT="3.1.2 Component based software engineering (CBSE)" ID="ID_683682453" CREATED="1462856089155" MODIFIED="1462856089155" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/Self-adaptive%20systems-%20A%20survey%20of%20current%20approaches,%20research%20challenges%20and%20applications.pdf">
<pdf_annotation type="BOOKMARK" page="3" object_id="4753707466315189930" object_number="1249" document_hash="2510D7C6FC9B18AD27E31D162CA1B4117B87AF42285487BEAE1BA2BBEDE1294E">
    <pdf_title>Expert Systems with Applications</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="MaciasEscriva20137267"/>
<attribute NAME="journal" VALUE="Expert Systems with Applications "/>
<attribute NAME="authors" VALUE="Frank D. Mac&#xed;as-Escriv&#xe1; and Rodolfo Haber and Raul del Toro and Vicente Hernandez"/>
<attribute NAME="title" VALUE="Self-adaptive systems: A survey of current approaches, research challenges and applications "/>
<attribute NAME="year" VALUE="2013"/>
</node>
</node>
<node TEXT="CBSE: reusable adaptation engine" ID="ID_85511697" CREATED="1461792034941" MODIFIED="1463339055476" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/Self-Adaptive%20Software-%20Landscape%20and%20Research%20Challenges.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="18" object_id="5618031938849056142" object_number="290" document_hash="88C686A0379C463E5D6C4D6A474F17E3B625A7C101DD49EC3B7FF2BB321F">
    <pdf_title>{}</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="salehie2009"/>
<attribute NAME="journal" VALUE="ACM Transactions on Autonomous and Adaptive Systems (TAAS)"/>
<attribute NAME="authors" VALUE="Salehie, Mazeiar and Tahvildari, Ladan"/>
<attribute NAME="title" VALUE="Self-adaptive software: Landscape and research challenges"/>
<attribute NAME="year" VALUE="2009"/>
</node>
<node TEXT="Decentralized applied through independent components coordinate one another" ID="ID_826254082" CREATED="1461951469026" MODIFIED="1461951469027" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/Software%20Engineering%20for%20Self-Adaptive%20Systems-%20A%202nd%20Research%20Roadmap.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="15" object_id="7829881936978129204" object_number="194" document_hash="711C9B4B38ABD6D245B6CBA99A7C6BD6B4A75EFBCEA42E49DCFAB3D416F51D">
    <pdf_title>Software Engineering for Self-Adaptive Systems: AS e c o n dR e s e a r c hR o a d m a p</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="Lemos13"/>
<attribute NAME="authors" VALUE="Rog{\&apos;{e}}rio de Lemos and Holger Giese and Hausi A. M{\&quot;{u}}ller and Mary Shaw and Jesper Andersson and Luciano Baresi and Basil Becker and Nelly Bencomo and Yuriy Brun and Bojan Cukic and Ron Desmarais and Schahram Dustdar and Gregor Engels and Kurt Geihs and Karl M. Goeschka and Alessandra Gorla and Vincenzo Grassi and Paola Inverardi and Gabor Karsai and Jeff Kramer and Marin Litoiu and Antonia Lopes and Jeff Magee and Sam Malek and Serge Mankovskii and Raffaela Mirandola and John Mylopoulos and Oscar Nierstrasz and Mauro Pezz{\`{e}} and Christian Prehofer and Wilhelm Sch{\&quot;{a}}fer and Rick Schlichting and Bradley Schmerl and Dennis B. Smith and Jo{\~{a}}o P. Sousa and Gabriel Tamura and Ladan Tahvildari and Norha M. Villegas and Thomas Vogel and Danny Weyns and Kenny Wong and Jochen Wuttke"/>
<attribute NAME="title" VALUE="Software engineering for self-adaptive systems: A second research roadmap"/>
<attribute NAME="year" VALUE="2013"/>
</node>
<node TEXT="There is a need for practical and effective techniques for decentralized control of self-adaptive software" ID="ID_1999473525" CREATED="1461951469157" MODIFIED="1461951469157" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/Software%20Engineering%20for%20Self-Adaptive%20Systems-%20A%202nd%20Research%20Roadmap.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="16" object_id="4022552627499216040" object_number="213" document_hash="711C9B4B38ABD6D245B6CBA99A7C6BD6B4A75EFBCEA42E49DCFAB3D416F51D">
    <pdf_title>Software Engineering for Self-Adaptive Systems: AS e c o n dR e s e a r c hR o a d m a p</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="Lemos13"/>
<attribute NAME="authors" VALUE="Rog{\&apos;{e}}rio de Lemos and Holger Giese and Hausi A. M{\&quot;{u}}ller and Mary Shaw and Jesper Andersson and Luciano Baresi and Basil Becker and Nelly Bencomo and Yuriy Brun and Bojan Cukic and Ron Desmarais and Schahram Dustdar and Gregor Engels and Kurt Geihs and Karl M. Goeschka and Alessandra Gorla and Vincenzo Grassi and Paola Inverardi and Gabor Karsai and Jeff Kramer and Marin Litoiu and Antonia Lopes and Jeff Magee and Sam Malek and Serge Mankovskii and Raffaela Mirandola and John Mylopoulos and Oscar Nierstrasz and Mauro Pezz{\`{e}} and Christian Prehofer and Wilhelm Sch{\&quot;{a}}fer and Rick Schlichting and Bradley Schmerl and Dennis B. Smith and Jo{\~{a}}o P. Sousa and Gabriel Tamura and Ladan Tahvildari and Norha M. Villegas and Thomas Vogel and Danny Weyns and Kenny Wong and Jochen Wuttke"/>
<attribute NAME="title" VALUE="Software engineering for self-adaptive systems: A second research roadmap"/>
<attribute NAME="year" VALUE="2013"/>
</node>
</node>
<node TEXT="Applications of adaptivity in GUI" ID="ID_1707880067" CREATED="1463251516347" MODIFIED="1463251525776">
<node TEXT="reuse in the UI domain is typically limited to toolkit (widget) code" ID="ID_58546014" CREATED="1462990763751" MODIFIED="1462990763752" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/user-interface/A%20Component-%20and%20Message-Based%20Architectural%20Style%20for%20GUI%20Software.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="1" object_id="3690877350081033102" object_number="12" document_hash="2F401AA0F441D53C27DBE5867FB17E1EDE17835CD3C9E2C9181EA34F2B8">
    <pdf_title>A Component- and Message-Based Architectural Style for GUI Software</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="taylor1996component"/>
<attribute NAME="journal" VALUE="Software Engineering, IEEE Transactions on"/>
<attribute NAME="authors" VALUE="Taylor, Richard N and Medvidovic, Nenad and Anderson, Kenneth M and Whitehead Jr, E James and Robbins, Jason E and Nies, Kari A and Oreizy, Peyman and Dubrow, Deborah L"/>
<attribute NAME="title" VALUE="A component-and message-based architectural style for GUI software"/>
<attribute NAME="year" VALUE="1996"/>
</node>
<node TEXT="Components written in multiple languages, architectures changing dynamically, multiple toolkits, multi-users and media" ID="ID_1913592923" CREATED="1462990763788" MODIFIED="1462990763789" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/user-interface/A%20Component-%20and%20Message-Based%20Architectural%20Style%20for%20GUI%20Software.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="1" object_id="4699668183143116127" object_number="13" document_hash="2F401AA0F441D53C27DBE5867FB17E1EDE17835CD3C9E2C9181EA34F2B8">
    <pdf_title>A Component- and Message-Based Architectural Style for GUI Software</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="taylor1996component"/>
<attribute NAME="journal" VALUE="Software Engineering, IEEE Transactions on"/>
<attribute NAME="authors" VALUE="Taylor, Richard N and Medvidovic, Nenad and Anderson, Kenneth M and Whitehead Jr, E James and Robbins, Jason E and Nies, Kari A and Oreizy, Peyman and Dubrow, Deborah L"/>
<attribute NAME="title" VALUE="A component-and message-based architectural style for GUI software"/>
<attribute NAME="year" VALUE="1996"/>
</node>
<node TEXT="Components to be multi-platform from definition" ID="ID_1660343028" CREATED="1463251695990" MODIFIED="1463251724726">
<node TEXT="Definition of computing platform = {hardware, OS, computing capability, UI toolkit}" ID="ID_471760124" CREATED="1462990764612" MODIFIED="1462990764612" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/user-interface/Multiple%20user%20interfaces%20and%20cross-platform%20UX.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="7" object_id="7623938417870946008" object_number="73" document_hash="E6754AF841AE9BCFF0B5A4E0601E5B46200ABB924584D36E671507F5E115349">
    <pdf_title>MUIC- PUE:TF</pdf_title>
</pdf_annotation>
</node>
</node>
</node>
<node TEXT="Tools to design adaptive software: Human interface" ID="ID_576939944" CREATED="1463251918719" MODIFIED="1463251934809">
<node TEXT="No human interface" ID="ID_119021669" CREATED="1461792034973" MODIFIED="1463339055509" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/Self-Adaptive%20Software-%20Landscape%20and%20Research%20Challenges.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="26" object_id="6928591266134477341" object_number="350" document_hash="88C686A0379C463E5D6C4D6A474F17E3B625A7C101DD49EC3B7FF2BB321F">
    <pdf_title>{}</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="salehie2009"/>
<attribute NAME="journal" VALUE="ACM Transactions on Autonomous and Adaptive Systems (TAAS)"/>
<attribute NAME="authors" VALUE="Salehie, Mazeiar and Tahvildari, Ladan"/>
<attribute NAME="title" VALUE="Self-adaptive software: Landscape and research challenges"/>
<attribute NAME="year" VALUE="2009"/>
</node>
</node>
<node TEXT="What is a self-adaptive system" ID="ID_63823141" CREATED="1463252076503" MODIFIED="1463252091211">
<node TEXT="Life-cycle not stopped after its development" ID="ID_1966387203" CREATED="1461791732211" MODIFIED="1463339055499" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/Self-Adaptive%20Software-%20Landscape%20and%20Research%20Challenges.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="4" object_id="8897994094060692652" object_number="58" document_hash="88C686A0379C463E5D6C4D6A474F17E3B625A7C101DD49EC3B7FF2BB321F">
    <pdf_title>{}</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="salehie2009"/>
<attribute NAME="journal" VALUE="ACM Transactions on Autonomous and Adaptive Systems (TAAS)"/>
<attribute NAME="authors" VALUE="Salehie, Mazeiar and Tahvildari, Ladan"/>
<attribute NAME="title" VALUE="Self-adaptive software: Landscape and research challenges"/>
<attribute NAME="year" VALUE="2009"/>
</node>
</node>
</node>
<node TEXT="Challenges" POSITION="left" ID="ID_1232764424" CREATED="1463334231733" MODIFIED="1463334263492">
<edge COLOR="#0000ff"/>
<node TEXT="Scalability" ID="ID_900438827" CREATED="1463334265291" MODIFIED="1463334277290">
<node TEXT="Local control has scalability problems because all must be processed at a single point" ID="ID_1143049888" CREATED="1461951469223" MODIFIED="1461951469224" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/Software%20Engineering%20for%20Self-Adaptive%20Systems-%20A%202nd%20Research%20Roadmap.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="17" object_id="3337570913916782556" object_number="233" document_hash="711C9B4B38ABD6D245B6CBA99A7C6BD6B4A75EFBCEA42E49DCFAB3D416F51D">
    <pdf_title>Software Engineering for Self-Adaptive Systems: AS e c o n dR e s e a r c hR o a d m a p</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="Lemos13"/>
<attribute NAME="authors" VALUE="Rog{\&apos;{e}}rio de Lemos and Holger Giese and Hausi A. M{\&quot;{u}}ller and Mary Shaw and Jesper Andersson and Luciano Baresi and Basil Becker and Nelly Bencomo and Yuriy Brun and Bojan Cukic and Ron Desmarais and Schahram Dustdar and Gregor Engels and Kurt Geihs and Karl M. Goeschka and Alessandra Gorla and Vincenzo Grassi and Paola Inverardi and Gabor Karsai and Jeff Kramer and Marin Litoiu and Antonia Lopes and Jeff Magee and Sam Malek and Serge Mankovskii and Raffaela Mirandola and John Mylopoulos and Oscar Nierstrasz and Mauro Pezz{\`{e}} and Christian Prehofer and Wilhelm Sch{\&quot;{a}}fer and Rick Schlichting and Bradley Schmerl and Dennis B. Smith and Jo{\~{a}}o P. Sousa and Gabriel Tamura and Ladan Tahvildari and Norha M. Villegas and Thomas Vogel and Danny Weyns and Kenny Wong and Jochen Wuttke"/>
<attribute NAME="title" VALUE="Software engineering for self-adaptive systems: A second research roadmap"/>
<attribute NAME="year" VALUE="2013"/>
</node>
<node TEXT="trade-off that is likely to occur is between scalability and performance: Will the threshold of scalability with respect to performance be reached, and when?" ID="ID_371630544" CREATED="1462990764467" MODIFIED="1462990764468" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/user-interface/A%20Component-%20and%20Message-Based%20Architectural%20Style%20for%20GUI%20Software.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="9" object_id="3123265056646541348" object_number="100" document_hash="2F401AA0F441D53C27DBE5867FB17E1EDE17835CD3C9E2C9181EA34F2B8">
    <pdf_title>A Component- and Message-Based Architectural Style for GUI Software</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="taylor1996component"/>
<attribute NAME="journal" VALUE="Software Engineering, IEEE Transactions on"/>
<attribute NAME="authors" VALUE="Taylor, Richard N and Medvidovic, Nenad and Anderson, Kenneth M and Whitehead Jr, E James and Robbins, Jason E and Nies, Kari A and Oreizy, Peyman and Dubrow, Deborah L"/>
<attribute NAME="title" VALUE="A component-and message-based architectural style for GUI software"/>
<attribute NAME="year" VALUE="1996"/>
</node>
</node>
</node>
<node TEXT="Existing solutions" POSITION="right" ID="ID_407546217" CREATED="1463334741223" MODIFIED="1463334749797">
<edge COLOR="#00ff00"/>
<node TEXT="DEUCE" ID="ID_1544544171" CREATED="1463334751883" MODIFIED="1463334756403">
<node TEXT="Advantages" ID="ID_1555387250" CREATED="1463334762694" MODIFIED="1463334774135">
<node TEXT="Adaptation is part of the UI design (explicit), which is a requirement for an Adaptive Component where the control data is the data used in rules/policies" ID="ID_1574855123" CREATED="1463334855660" MODIFIED="1463335278117">
<node TEXT="make explicit the fact that the behaviour of a com- ponent depends on some well identified control data" ID="ID_1691352483" CREATED="1463152387889" MODIFIED="1463152387889" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/adaptive%20software/A%20Conceptual%20Framework%20for%20Adaptation.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="3" object_id="1148491147974264242" object_number="70" document_hash="51CFA4A82390A534C119B835733F4E56A4CCADAA474D86C526BAF7D1C6EA67">
    <pdf_title>AC o n c e p t u a lF r a m e w o r kf o rA d a p t a t i o n</pdf_title>
</pdf_annotation>
<attribute NAME="key" VALUE="Bruni2012"/>
<attribute NAME="authors" VALUE="Bruni, Roberto and Corradini, Andrea and Gadducci, Fabio and Lluch Lafuente, Alberto and Vandin, Andrea"/>
<attribute NAME="title" VALUE="Fundamental Approaches to Software Engineering: 15th International Conference, FASE 2012, Held as Part of the European Joint Conferences on Theory and Practice of Software, ETAPS 2012, Tallinn, Estonia, March 24 - April 1, 2012. Proceedings"/>
<attribute NAME="year" VALUE="2012"/>
</node>
</node>
<node TEXT="Intuitive humain interface and rules (leftOf, above, etc.), design using an already existing front-end" ID="ID_1958865642" CREATED="1463334871144" MODIFIED="1463335338351"/>
<node TEXT="Separation of concerns is well established" ID="ID_818623764" CREATED="1463335464021" MODIFIED="1463335478621"/>
<node TEXT="Connection logic used as an adapter between application and ui logic" ID="ID_602986576" CREATED="1463335957126" MODIFIED="1463335978326"/>
</node>
<node TEXT="Shortcomings" ID="ID_1700677434" CREATED="1463334782309" MODIFIED="1463334790975">
<node TEXT="Constraints are aimed at layouting UI components" ID="ID_1472934570" CREATED="1463334828631" MODIFIED="1463336100896"/>
<node TEXT="Parametric is the main adaptive behavior implemented, substituion isn&apos;t obvious" ID="ID_1773177691" CREATED="1463335361497" MODIFIED="1463336338956"/>
<node TEXT="Rules can become hard to write because: 1) no tool provided 2) developers unfamiliar" ID="ID_669359718" CREATED="1463335556413" MODIFIED="1463335587590"/>
<node TEXT="Central adaptation" ID="ID_465147270" CREATED="1463336371924" MODIFIED="1463336377353"/>
<node TEXT="Transforming a static software to an adaptive one requires significant changes at once" ID="ID_1851137268" CREATED="1463336405869" MODIFIED="1463336455349"/>
</node>
<node TEXT="Future work proposed" ID="ID_659623976" CREATED="1463334795485" MODIFIED="1463334804240">
<node TEXT="Future Research Directions" ID="ID_476971895" CREATED="1461700762838" MODIFIED="1461700762838" LINK="project://15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB/literature_repository/user-interface/vub-prog-phd-08-02.pdf">
<pdf_annotation type="BOOKMARK" page="175" object_id="2004639922106991183" object_number="3034" document_hash="45AF1A3BD546466FFEEA666AD4A56B6CFAC85E98ECFA471CB8C319E62AF7AD2">
    <pdf_title>On the Separation of User Interface Concerns</pdf_title>
</pdf_annotation>
<attribute NAME="authors" VALUE="Goderis, Sofie"/>
<attribute NAME="title" VALUE="On the Separation of User Interface Concerns"/>
<attribute NAME="year" VALUE="2008"/>
<attribute NAME="key" VALUE="goderis2008separation"/>
</node>
</node>
</node>
<node TEXT="Systems with central control" ID="ID_1475962810" CREATED="1463336230160" MODIFIED="1463336241279">
<node TEXT="Scalability issues" ID="ID_1072374801" CREATED="1463336245888" MODIFIED="1463336391771"/>
<node TEXT="Harder to move from static to adaptive" ID="ID_452362390" CREATED="1463336393159" MODIFIED="1463336399912"/>
</node>
<node TEXT="Intel Context Sensing SDK" ID="ID_1018942984" CREATED="1463442662016" MODIFIED="1463442672586"/>
</node>
</node>
</map>
