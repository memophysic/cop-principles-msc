<map version="docear 1.1" dcr_id="1464802908099_4siqirgphdytoqnw9gjr44dl6" project="15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB" project_last_home="file:/Users/memo/Docear/projects/cop-principles-msc/">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="summary_sca701" FOLDED="false" ID="ID_458918870" CREATED="1464802908038" MODIFIED="1464802908041">
<hook NAME="AutomaticEdgeColor" COUNTER="6"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Who?" POSITION="right" ID="ID_1099714464" CREATED="1464802916342" MODIFIED="1464802920379">
<edge COLOR="#ff0000"/>
<node TEXT="Me" ID="ID_309525072" CREATED="1464803331573" MODIFIED="1464803335044"/>
</node>
<node TEXT="What?" POSITION="right" ID="ID_291502200" CREATED="1464802926973" MODIFIED="1464802930288">
<edge COLOR="#0000ff"/>
<node TEXT="Repository of patterns for adaptive software, especially useful for Gui" ID="ID_107129355" CREATED="1464803339722" MODIFIED="1464803360039">
<node TEXT="Monitors" ID="ID_173538383" CREATED="1464803366948" MODIFIED="1464803368635"/>
<node TEXT="Adaptive Component" ID="ID_368343748" CREATED="1464803369806" MODIFIED="1464803373759"/>
</node>
<node TEXT="Primitive concepts of adaptive software" ID="ID_1439127267" CREATED="1464803361472" MODIFIED="1464803404262">
<node TEXT="Adaptation space" ID="ID_145920510" CREATED="1464803405888" MODIFIED="1464803411012"/>
<node TEXT="Adaptation data" ID="ID_345725822" CREATED="1464803412704" MODIFIED="1464803414793"/>
<node TEXT="Monitoring group" ID="ID_366775214" CREATED="1464803415633" MODIFIED="1464803431618"/>
</node>
<node TEXT="Decentralized approach implemented iteratively" ID="ID_843359076" CREATED="1464803434350" MODIFIED="1464803446131">
<node TEXT="Guidelines for implementation of patterns in the development process" ID="ID_1224161152" CREATED="1464803448506" MODIFIED="1464803467599"/>
<node TEXT="Control approaches adapted to the complexity of the application" ID="ID_1856935963" CREATED="1464803469229" MODIFIED="1464803484290"/>
<node TEXT="Addition of adaptative behavior shouldn&apos;t affect other parts of a system" ID="ID_1417701505" CREATED="1464803485669" MODIFIED="1464803510896"/>
</node>
<node TEXT="Prototype demonstrating use of concepts" ID="ID_1264649814" CREATED="1464803600141" MODIFIED="1464803607286">
<node TEXT="Distributed Gui over" ID="ID_1207878143" CREATED="1464803615253" MODIFIED="1464803619466">
<node TEXT="Platforms" ID="ID_1764009513" CREATED="1464803624987" MODIFIED="1464803626916">
<node TEXT="Toolkit" ID="ID_1061503110" CREATED="1464803634985" MODIFIED="1464803637163"/>
<node TEXT="OS" ID="ID_1399024313" CREATED="1464803638809" MODIFIED="1464803639858"/>
<node TEXT="Processing capabilities" ID="ID_869645229" CREATED="1464803640011" MODIFIED="1464803662311"/>
<node TEXT="Hardware components" ID="ID_254070856" CREATED="1464803664736" MODIFIED="1464803673209">
<node TEXT="Input" ID="ID_942113451" CREATED="1464803709701" MODIFIED="1464803711420">
<node TEXT="Mouse" ID="ID_1795237265" CREATED="1464803744140" MODIFIED="1464803745352"/>
<node TEXT="Touch" ID="ID_1787596329" CREATED="1464803746656" MODIFIED="1464803750891"/>
<node TEXT="Keyboard" ID="ID_742038066" CREATED="1464803752170" MODIFIED="1464803753916"/>
<node TEXT="Controller (e.g. Xbox)" ID="ID_89366231" CREATED="1464803757281" MODIFIED="1464803766113"/>
</node>
<node TEXT="Output" ID="ID_168681480" CREATED="1464803712768" MODIFIED="1464803714376">
<node TEXT="Screen" ID="ID_1209488933" CREATED="1464803715732" MODIFIED="1464803719383">
<node TEXT="Resolution" ID="ID_1952689687" CREATED="1464803732090" MODIFIED="1464803740444"/>
<node TEXT="Dpi" ID="ID_1340694552" CREATED="1464803734427" MODIFIED="1464803735889"/>
</node>
</node>
</node>
</node>
<node TEXT="Process" ID="ID_1739513105" CREATED="1464803681398" MODIFIED="1464803693160">
<node TEXT="Core" ID="ID_1810402527" CREATED="1464803698184" MODIFIED="1464803699563"/>
<node TEXT="Backend" ID="ID_161290606" CREATED="1464803700625" MODIFIED="1464803702171"/>
</node>
</node>
</node>
</node>
<node TEXT="Where?" POSITION="right" ID="ID_1952934594" CREATED="1464802930781" MODIFIED="1464802934797">
<edge COLOR="#00ff00"/>
<node TEXT="GitLab" ID="ID_1777148276" CREATED="1464803525820" MODIFIED="1464803555737">
<node TEXT="Iteration report (LaTeX)" ID="ID_1826327984" CREATED="1464803562134" MODIFIED="1464803568114"/>
<node TEXT="Library (Python)" ID="ID_462079896" CREATED="1464803570108" MODIFIED="1464803573501"/>
<node TEXT="Major project (Python, PySide, PyGtk)" ID="ID_1948733700" CREATED="1464803575359" MODIFIED="1464803591350"/>
</node>
</node>
<node TEXT="Why?" POSITION="left" ID="ID_1356769234" CREATED="1464802937450" MODIFIED="1471575492730">
<edge COLOR="#ff00ff"/>
<node TEXT="Need for techniques for developping adaptive software easily" ID="ID_1225639100" CREATED="1464803260570" MODIFIED="1464803277672"/>
<node TEXT="Need for easily understandable concepts and tools" ID="ID_1525672747" CREATED="1464803279409" MODIFIED="1464803290941"/>
<node TEXT="Need for improvement to efficiently implement Gui in evolutive way" ID="ID_299110865" CREATED="1464803291292" MODIFIED="1464803328868"/>
</node>
<node TEXT="How?" POSITION="left" ID="ID_1389795472" CREATED="1464802940702" MODIFIED="1464802944638">
<edge COLOR="#00ffff"/>
<node TEXT="Python or Java sample programs" ID="ID_847471281" CREATED="1464803071625" MODIFIED="1464803093983">
<node TEXT="Many small sample code (&lt; 500 lines)" ID="ID_555376664" CREATED="1464803211566" MODIFIED="1464803226991"/>
<node TEXT="One large Gui application" ID="ID_1204680545" CREATED="1464803230302" MODIFIED="1464803243139">
<node TEXT="Toolkits used" ID="ID_472102106" CREATED="1464803244439" MODIFIED="1464803250365">
<node TEXT="Gtk" ID="ID_556887667" CREATED="1464803252052" MODIFIED="1464803253102"/>
<node TEXT="Qt" ID="ID_1871635838" CREATED="1464803253997" MODIFIED="1464803254944"/>
</node>
</node>
</node>
<node TEXT="Analysis of previous approaches and patterns" ID="ID_938549571" CREATED="1464803182555" MODIFIED="1464803196381">
<node TEXT="Comparison of metrics, architecture complexity" ID="ID_93786042" CREATED="1464803198297" MODIFIED="1464803209423"/>
</node>
<node TEXT="Performance checks through code analysis tools" ID="ID_820438598" CREATED="1464803098896" MODIFIED="1464803113045">
<node TEXT="Metrics" ID="ID_1617887633" CREATED="1464803115891" MODIFIED="1464803117405"/>
<node TEXT="Radon" ID="ID_248382332" CREATED="1464803119942" MODIFIED="1464803136356"/>
<node TEXT="Pyformance" ID="ID_1530318686" CREATED="1464803175561" MODIFIED="1464803178209"/>
</node>
</node>
<node TEXT="When?" POSITION="left" ID="ID_475750778" CREATED="1464802945362" MODIFIED="1464802948945">
<edge COLOR="#ffff00"/>
<node TEXT="Over the course of 1 year" ID="ID_1849824100" CREATED="1464802951544" MODIFIED="1464802956305">
<node TEXT="Summer 2016" ID="ID_1054227689" CREATED="1464802957967" MODIFIED="1464802963559">
<node TEXT="First prototype, sample cases" ID="ID_1971985717" CREATED="1464803012167" MODIFIED="1464803042077"/>
</node>
<node TEXT="Fall 2016" ID="ID_435774571" CREATED="1464802967489" MODIFIED="1464802978064">
<node TEXT="Formal list of patterns, major project" ID="ID_749078870" CREATED="1464803043813" MODIFIED="1464803053758"/>
</node>
<node TEXT="Winter 2016" ID="ID_413644539" CREATED="1464802978566" MODIFIED="1464802981867">
<node TEXT="Final memoir writing" ID="ID_25084109" CREATED="1464803055273" MODIFIED="1464803069838"/>
</node>
</node>
</node>
</node>
</map>
