<map version="docear 1.1" dcr_id="1467306015110_ck3rbv3rkv8948pxd6ayibzo" project="15453FCCC8E6MKJ6PK9RJJ6B5DZFUGDYCFDB" project_last_home="file:/Users/memo/Docear/projects/cop-principles-msc/">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="article1" FOLDED="false" ID="ID_1430295729" CREATED="1467306015047" MODIFIED="1467306015050">
<hook NAME="AutomaticEdgeColor" COUNTER="10"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Introduction" POSITION="right" ID="ID_308005584" CREATED="1467306028339" MODIFIED="1467306035422">
<edge COLOR="#ff0000"/>
<node TEXT="Adaptive software" ID="ID_595321312" CREATED="1467306173152" MODIFIED="1467306203298">
<node TEXT="Generic framework exist" ID="ID_1144061710" CREATED="1467306280650" MODIFIED="1467306295614"/>
<node TEXT="Life-cycle not stopped after its development &amp; initial setup" ID="ID_500989291" CREATED="1467306308752" MODIFIED="1467306320360"/>
</node>
<node TEXT="Variety of platforms" ID="ID_1487379376" CREATED="1467306206984" MODIFIED="1467306213996"/>
<node TEXT="Adapt human interfaces" ID="ID_702352768" CREATED="1467306214393" MODIFIED="1467306218561"/>
<node TEXT="GUI" ID="ID_1715345195" CREATED="1467306228272" MODIFIED="1467306253719">
<node TEXT="Implementation of adaptation in GUI" ID="ID_424287187" CREATED="1467306264949" MODIFIED="1467306273120"/>
</node>
<node TEXT="CBSE" ID="ID_974060963" CREATED="1467306338691" MODIFIED="1467306344066">
<node TEXT="CBSE as toolkits in GUI" ID="ID_1844700648" CREATED="1467306345797" MODIFIED="1467306351927"/>
</node>
<node TEXT="Contributions" ID="ID_472433711" CREATED="1467306785982" MODIFIED="1467306794508">
<node TEXT="Design patterns" ID="ID_613723234" CREATED="1467306795488" MODIFIED="1467306801458"/>
<node TEXT="Prototype" ID="ID_579184929" CREATED="1467306801870" MODIFIED="1467306810856"/>
<node TEXT="Gains analysis" ID="ID_8623076" CREATED="1467306811895" MODIFIED="1467306832057"/>
<node TEXT="Workflow description" ID="ID_1871252803" CREATED="1467306832389" MODIFIED="1467306843027"/>
</node>
</node>
<node TEXT="Concepts" POSITION="right" ID="ID_1103395614" CREATED="1467306225259" MODIFIED="1467306947562">
<edge COLOR="#007c00"/>
<node TEXT="Context and self adaptation data" ID="ID_241227421" CREATED="1467306949577" MODIFIED="1467306960413"/>
</node>
</node>
</map>
